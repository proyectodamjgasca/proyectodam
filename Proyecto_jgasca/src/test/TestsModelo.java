package test;

import Encriptacion.Encriptar;
import com.javiergc.Proyecto.base.*;
import com.javiergc.Proyecto.gui.Modelo;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase TestModelo que realiza prueba sobre el modelo de la aplicación
 */
public class TestsModelo {
    Modelo modelo ;

    /**
     * Constructor de la clase TestsModelo
     */
    public TestsModelo(){
        this.modelo=new Modelo();
        modelo.conectar();
    }

    /**
     * Test que comprueba si existe un usuario administrador
     */
    @Test
    void existeUsuarioAdministrador(){
        ArrayList<Tecnico> tecnicos = modelo.getTecnicos();
        boolean actual = false;
        for (Tecnico tecnico: tecnicos) {
            try {
                if (Encriptar.desencriptar(tecnico.getPermisos()).equals("153")){
                    actual=true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Assertions.assertTrue(actual);
    }

    /**
     * Test que comprueba si el metodo isNumeric funciona
     */
    @Test
    void funcionalidadIsNumeric(){
        boolean actual = modelo.isNumeric("123456");
        Assertions.assertTrue(actual);
    }

    /**
     * Test que comprueba si el metodo guardarObjeto funciona
     */
    @Test
    void guardarObjeto(){
        Cliente cliente = new Cliente();
        cliente.setNombre("Pedro");
        cliente.setDni("123456789");
        cliente.setFechaAlta(LocalDate.now());
        int numeroClientesEsperado = modelo.getClientes().size() + 1;
        modelo.guardarObjeto(cliente);
        int numeroClientesActual = modelo.getClientes().size();
        modelo.borrarUsuarioPorDni(cliente.getDni());

        Assertions.assertEquals(numeroClientesEsperado,numeroClientesActual);
    }

    /**
     * Comprueba que el metodo objectToDocument funciona
     */
    @Test
    void objectToDocument(){
        Cliente cliente = new Cliente();
        cliente.setNombre("Pedro");
        cliente.setDni("123456789");
        cliente.setFechaAlta(LocalDate.now());

        boolean actual = false;
        if (modelo.objectToDocument(cliente) instanceof Document){
            actual = true;
        }
        Assertions.assertTrue(actual);
    }

    /**
     * Test que comprueba que el metodo documentToCliente funciona
     */
    @Test
    void documentToCliente(){
        Cliente cliente = new Cliente();
        cliente.setNombre("Pedro");
        cliente.setDni("123456789");
        cliente.setFechaAlta(LocalDate.now());

        Document clienteDocu = modelo.objectToDocument(cliente);
        boolean actual = true;
        if (!modelo.documentToCliente(clienteDocu).equals(cliente)){
            actual =false;
        }
        Assertions.assertFalse(actual);
    }

    /**
     * Metodo que comprueba que el metodo documentToTecnico funciona correctamento
     */
    @Test
    void documentToTecnico(){
        Tecnico Tecnico = new Tecnico();
        Tecnico.setNombre("Pedro");
        Tecnico.setDni("123456789");
        Tecnico.setFechaAlta(LocalDate.now());

        Document TecnicoDocu = modelo.objectToDocument(Tecnico);
        boolean actual = true;
        if (!modelo.documentToTecnico(TecnicoDocu).equals(Tecnico)){
            actual =false;
        }
        Assertions.assertFalse(actual);
    }

    /**
     * Metodo que comprueba que el metodo documentToAdministrador funciona correctamento
     */
    @Test
    void documentToAdministrador(){
        Administrador admin = new Administrador();
        admin.setNombre("Pedro");
        admin.setDni("123456789");
        admin.setFechaAlta(LocalDate.now());

        Document AdminDocu = modelo.objectToDocument(admin);
        boolean actual = true;
        if (!modelo.documentToAdministrador(AdminDocu).equals(admin)){
            actual =false;
        }
        Assertions.assertFalse(actual);
    }

    /**
     * Metodo que comprueba que el metodo documentToIncidencia funciona correctamento
     */
    @Test
    void documentToIncidencia(){
        Incidencia incidencia = new Incidencia();
        incidencia.setFecha_apertura(LocalDate.now());

        Document IncidenciaDocu = modelo.objectToDocument(incidencia);
        boolean actual = true;
        if (!modelo.documentToIncidencia(IncidenciaDocu).equals(incidencia)){
            actual =false;
        }
        Assertions.assertFalse(actual);
    }

    /**
     * Metodo que comprueba que el metodo documentToDepartamento funciona correctamento
     */
    @Test
    void documentToDepartamento(){
        Departamento departamento = new Departamento();
        departamento.setNombre("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        Document DepartamentoDocu = modelo.objectToDocument(departamento);
        boolean actual = true;
        if (!modelo.documentToDepartamento(DepartamentoDocu).equals(departamento)){
            actual =false;
        }
        Assertions.assertFalse(actual);
    }

    /**
     * Test que comprueba si la base de datos devuelve Incidencias
     */
    @Test
    void getIncidencias(){
        int actual = modelo.getIncidencias().size();
        int noEsperado = 0;
        Assertions.assertFalse(actual == noEsperado);
    }

    /**
     * Test que comprueba si la base de datos devuelve clientes
     */
    @Test
    void obtenerClientes(){
        int actual = modelo.getClientes().size();
        int noEsperado = 0;
        Assertions.assertFalse(actual == noEsperado);
    }

    /**
     * Test que comprueba si la base de datos devuelve Tecnicos
     */
    @Test
    void obtenerTecnicos(){
        int actual = modelo.getTecnicos().size();
        int noEsperado = 0;
        Assertions.assertFalse(actual == noEsperado);
    }

    /**
     * Test que comprueba si la base de datos devuelve Departamento
     */
    @Test
    void obtenerDepartamentos(){
        int actual = modelo.getDepartamentos().size();
        int noEsperado = 0;
        Assertions.assertFalse(actual == noEsperado);
    }

    /**
     * Test que comprueba si el metodo getDepartamentosReparacion funciona correctamente
     */
    @Test
    void getDepartamentosReparacion(){
        ArrayList<Departamento> lista = new ArrayList<>();
        lista = modelo.getDepartamentosReparacion();
        boolean actual = false;
        if (lista == null){
            actual = true;
        }
        for (Departamento departamento : lista) {
            if (departamento.getReparacion().equals("no")){
                actual = true;
            }
        }
        Assertions.assertFalse(actual);
    }

    /**
     * Test que comprueba si el metodo getDepartamentosNoReparacion funciona correctamente
     */
    @Test
    void getDepartamentosNoReparacion(){
        ArrayList<Departamento> lista = new ArrayList<>();
        lista = modelo.getDepartamentosNoReparacion();
        boolean actual = false;
        if (lista == null){
            actual = true;
        }
        for (Departamento departamento : lista) {
            if (departamento.getReparacion().equals("si")){
                actual = true;
            }
        }
        Assertions.assertFalse(actual);
    }

    /**
     * Test que comprueba si el metodo borrarCliente borra un cliente
     */
    @Test
    void borrarCliente(){
        Cliente cliente = new Cliente();
        cliente.setNombre("Pedro");
        cliente.setDni("123456789");
        cliente.setFechaAlta(LocalDate.now());
        modelo.guardarObjeto(cliente);

        int esperado = modelo.getClientes().size() - 1;
        modelo.borrarCliente(cliente);
        int actual = modelo.getClientes().size();

        Assertions.assertEquals(esperado, actual);

    }

    /**
     * Test que comprueba si el metodo borrarTecnico funciona correctamente
     */
    @Test
    void borrarTecnico(){
        Tecnico tecnico = new Tecnico();
        tecnico.setNombre("Pedro");
        tecnico.setDni("123456789");
        tecnico.setFechaAlta(LocalDate.now());
        modelo.guardarObjeto(tecnico);

        int esperado = modelo.getTecnicos().size() - 1;
        modelo.borrarTecnico(tecnico);
        int actual = modelo.getTecnicos().size();

        Assertions.assertEquals(esperado, actual);
    }

    /**
     * Test que comprueba si el metodo borrarIncidencia funciona correctamente
     */
    @Test
    void borrarIncidencia(){
        Incidencia incidencia = new Incidencia();
        incidencia.setFecha_apertura(LocalDate.now());
        modelo.guardarObjeto(incidencia);

        int esperado = modelo.getIncidencias().size() - 1;
        modelo.borrarIncidencia(incidencia);
        int actual = modelo.getIncidencias().size();

        Assertions.assertEquals(esperado, actual);
    }

    /**
     * Test que comprueba si el metodo borrarUsuarioPorDni funciona correctamente
     */
    @Test
    void borrarUsuarioPorDni(){
        Cliente cliente = new Cliente();
        cliente.setNombre("Pedro");
        cliente.setDni("123456789");
        cliente.setFechaAlta(LocalDate.now());
        modelo.guardarObjeto(cliente);

        int esperado = modelo.getClientes().size() - 1;
        modelo.borrarUsuarioPorDni(cliente.getDni());
        int actual = modelo.getClientes().size();

        Assertions.assertEquals(esperado, actual);

    }

    /**
     * Test que comprueba si el metodo getUsuarios() funciona
     */
    @Test
    void getUsuarios(){
        Usuario usu = modelo.getClientes().get(0);
        int actual = modelo.getUsuarios(usu.getNombre(),true).size();
        Assertions.assertTrue(actual > 0);
    }

    /**
     * Test que comprueba si el metodo getDepartamentos funciona
     */
    @Test
    void getDepartamentos(){
        Departamento departamento = modelo.getDepartamentos().get(0);
        int actual = modelo.getDepartamentos(departamento.getNombre()).size();
        Assertions.assertTrue(actual > 0);
    }

    /**
     * Test que comprueba si el metodo buscarUsuarioPorDni funciona correctamente
     */
    @Test
    void buscarUsuarioPorDni(){
        Usuario usu = modelo.getClientes().get(0);
        int actual = modelo.buscarUsuarioPorDni(usu.getDni()).size();

        Assertions.assertTrue(actual == 1);
    }

    /**
     * Test que comprueba si el metodo buscarUsuarioPorId funciona correctamente
     */
    @Test
    void buscarUsuarioPorId(){
        Usuario usu = modelo.getClientes().get(0);
        int actual = modelo.buscarUsuarioPorId(usu.getId()).size();

        Assertions.assertTrue(actual == 1);
    }

    /**
     * Test que comprueba si el metodo buscarIncidenciaPorIdCliente funciona correctamente
     */
    @Test
    void buscarIncidenciaPorIdCliente(){
        ArrayList<Cliente> clientes = modelo.getClientes();
        boolean hayInciUser = false;
        Cliente usu = new Cliente();
        for (Cliente cliente :clientes) {
            int actual = modelo.buscarIncidenciaPorIdCliente(cliente.getId()).size();
            if (actual > 0){
                hayInciUser = true;
                usu = cliente;
            }
        }

        if (!hayInciUser){
            Assertions.assertTrue(false,"No se puede comprobar, no hay usuarios con incidencias");
        }else {
            int actual = modelo.buscarIncidenciaPorIdCliente(usu.getId()).size();
            Assertions.assertTrue(actual >= 1);
        }
    }

    /**
     * Test que comprueba si el metodo buscarIncidenciasUsuario funciona correctamente
     */
    @Test
    void buscarIncidenciasUsuario(){
        ArrayList<Cliente> clientes = modelo.getClientes();
        boolean hayInciUser = false;
        Cliente usu = new Cliente();
        for (Cliente cliente :clientes) {
            int actual = modelo.buscarIncidenciasUsuario(cliente.getNombre()).size();
            if (actual > 0){
                hayInciUser = true;
                usu = cliente;
            }
        }

        if (!hayInciUser){
            Assertions.assertTrue(false,"No se puede comprobar porque el test no tiene permisos de ningun tipo");
        }else {
            int actual = modelo.buscarIncidenciaPorIdCliente(usu.getId()).size();
            Assertions.assertTrue(actual >= 1);
        }
    }

    /**
     * Test que comprueba si el metodo borrarDepartamento funciona correctamente
     */
    @Test
    void borrarDepartamento(){
        Departamento departamento = new Departamento();
        departamento.setNombre("PruebaPruebaPruebaPruebaPruebaPrueba");
        departamento.setPlanta(-2);
        departamento.setReparacion("no");
        modelo.guardarObjeto(departamento);
        int esperado = modelo.getDepartamentos().size() - 1;
        modelo.borrarDepartamento(departamento.getNombre());
        int actual = modelo.getDepartamentos().size();

        Assertions.assertEquals(esperado,actual);
    }

    /**
     * Test que comprueba si el metodo buscarDepartamentoPorNombre funciona correctamente
     */
    @Test
    void buscarDepartamentoPorNombre(){
        Departamento esperado = modelo.getDepartamentos().get(0);
        Departamento actual = modelo.buscarDepartamentoPorNombre(esperado.getNombre());
        Assertions.assertEquals(esperado.getId(),actual.getId());
    }

    /**
     * Test que comprueba si el metodo buscarIncidenciaPorId funciona correctamente
     */
    @Test
    void buscarIncidenciaPorId(){
        Incidencia esperado = modelo.getIncidencias().get(0);
        Incidencia actual = modelo.buscarIncidenciaPorId(esperado.getId());
        Assertions.assertEquals(esperado.getId(),actual.getId());
    }

    /**
     * Test que comprueba si el metodo modificarCliente funciona correctamente
     */
    @Test
    void modificarCliente(){
        Cliente cliente = new Cliente();
        cliente.setNombre("Pedro");
        cliente.setDni("123456789");
        cliente.setFechaAlta(LocalDate.now());
        modelo.guardarObjeto(cliente);

        cliente = (Cliente) modelo.buscarUsuarioPorDni(cliente.getDni()).get(0);

        cliente.setNombre("Maria");
        modelo.modificarCliente(cliente);

        Cliente cliente1 = (Cliente) modelo.buscarUsuarioPorDni(cliente.getDni()).get(0);

        String  esperado = "Maria";
        modelo.borrarUsuarioPorDni(cliente.getDni());
        String actual = cliente1.getNombre();

        Assertions.assertTrue(esperado.equals(actual));
    }

    /**
     * Test que comprueba si el metodo modificarTecnico funciona correctamente
     */
    @Test
    void modificarTecnico(){
        Tecnico tecnico = new Tecnico();
        tecnico.setNombre("Pedro");
        tecnico.setDni("1234567899");
        tecnico.setFechaAlta(LocalDate.now());
        modelo.guardarObjeto(tecnico);

        tecnico = (Tecnico) modelo.buscarUsuarioPorDni(tecnico.getDni()).get(0);

        tecnico.setNombre("Maria");
        modelo.modificarTecnico(tecnico);

        Tecnico tecnico1 = (Tecnico) modelo.buscarUsuarioPorDni(tecnico.getDni()).get(0);

        String  esperado = "Maria";
        modelo.borrarUsuarioPorDni(tecnico.getDni());
        String actual = tecnico1.getNombre();

        Assertions.assertTrue(esperado.equals(actual));
    }

    /**
     * Test que comprueba si el metodo modificarAdmin funciona correctamente
     */
    @Test
    void modificarAdmin(){
        Administrador administrador = new Administrador();
        administrador.setNombre("Pedro");
        administrador.setDni("1234567898");
        administrador.setPermisosCifrado("153");
        administrador.setFechaAlta(LocalDate.now());
        modelo.guardarObjeto(administrador);

        administrador = (Administrador) modelo.buscarUsuarioPorDni(administrador.getDni()).get(0);

        administrador.setNombre("Maria");
        modelo.modificarAdmin(administrador);

        Administrador administrador1 = (Administrador) modelo.buscarUsuarioPorDni(administrador.getDni()).get(0);

        String  esperado = "Maria";
        modelo.borrarUsuarioPorDni(administrador.getDni());
        String actual = administrador1.getNombre();

        Assertions.assertTrue(esperado.equals(actual));
    }

    /**
     * Test que comprueba si el metodo  modificarIncidencia funciona correctamente
     */
    @Test
    void modificarIncidencia(){
        Incidencia incidencia = new Incidencia();
        incidencia.setFecha_apertura(LocalDate.now());
        incidencia.setAsunto("hola");
        modelo.guardarObjeto(incidencia);

        for (Incidencia incidencia1:modelo.getIncidencias()) {
            if (incidencia1.getId_cliente() == null){
                incidencia = incidencia1;
            }
        }

        incidencia.setAsunto("adios");
        modelo.modificarIncidencia(incidencia);

        String actual = modelo.buscarIncidenciaPorId(incidencia.getId()).getAsunto();
        modelo.borrarIncidencia(incidencia);
        String esperado = "adios";

        Assertions.assertEquals(esperado, actual);
    }

    /**
     * Test que comprueba si el metodo  modificarDepartamento funciona correctamente
     */
    @Test
    void modificarDepartamento(){
        Departamento departamento = new Departamento();
        departamento.setNombre("aaaaaaaaaaaa?aaaaaaaaaaaaaaaaaaaaa?");
        departamento.setPlanta(2);
        departamento.setReparacion("no");
        modelo.guardarObjeto(departamento);

        departamento = modelo.buscarDepartamentoPorNombre(departamento.getNombre());
        departamento.setPlanta(5);
        modelo.modificarDepartamento(departamento);

        String actual = modelo.buscarDepartamentoPorNombre(departamento.getNombre()).getPlanta() + "";
        modelo.borrarDepartamento(departamento);
        String esperado = "5";

        Assertions.assertEquals(esperado, actual);
    }

    /**
     * Test que comprueba si el metodo  getIncidenciasDepartamento funciona correctamente
     */
    @Test
    void getIncidenciasDepartamento(){
        Incidencia incidencia = new Incidencia();
        incidencia.setFecha_apertura(LocalDate.now());
        incidencia.setNombre_departamento("agibj?asvodsn");
        modelo.guardarObjeto(incidencia);
        modelo.guardarObjeto(incidencia);
        modelo.guardarObjeto(incidencia);
        int actual = modelo.getIncidenciasDepartamento("agibj?asvodsn").size();
        int esperado = 3;
        ArrayList<Incidencia> incidencias = modelo.getIncidenciasDepartamento("agibj?asvodsn");
        for (Incidencia incidencia1:incidencias) {
            modelo.borrarIncidencia(incidencia1);
        }
        Assertions.assertEquals(esperado,actual);
    }

    /**
     * Test que comprueba si el metodo  getIncidenciasDepartamentoReparacion funciona correctamente
     */
    @Test
    void getIncidenciasDepartamentoReparacion(){
        Incidencia incidencia = new Incidencia();
        incidencia.setFecha_apertura(LocalDate.now());
        incidencia.setNombre_departamento_reparacion("agibj?asvodsn");
        modelo.guardarObjeto(incidencia);
        modelo.guardarObjeto(incidencia);
        modelo.guardarObjeto(incidencia);
        int actual = modelo.getIncidenciasDepartamentoReparacion("agibj?asvodsn").size();
        int esperado = 3;
        ArrayList<Incidencia> incidencias = modelo.getIncidenciasDepartamentoReparacion("agibj?asvodsn");
        for (Incidencia incidencia1:incidencias) {
            modelo.borrarIncidencia(incidencia1);
        }
        Assertions.assertEquals(esperado,actual);
    }

    /**
     * Test que comprueba si el metodo  comprobarLogin funciona correctamente
     */
    @Test
    void comprobarLogin(){
        Cliente cliente = new Cliente();
        cliente.setNombre("Pedro");
        cliente.setDni("123456789999");
        cliente.setContrasenaHaseada("123456789");
        cliente.setFechaAlta(LocalDate.now());
        modelo.guardarObjeto(cliente);

        Usuario usu = modelo.comprobarLogin(cliente.getDni(),"123456789");
        modelo.borrarCliente((Cliente) modelo.buscarUsuarioPorDni("123456789999").get(0));
        Assertions.assertNotNull(usu);
    }
}
