package com.javiergc.Proyecto.base;

import Encriptacion.Encriptar;

import java.time.LocalDate;

/**
 * Clase Administrador
 */
public class Administrador extends Usuario {

    /**
     * Constructor de la clase Usuario
     *
     * @param nombre Nombre del administrador
     * @param apellidos Apellidos del administrador
     * @param email Email del administrador
     * @param telefono Telefono del administrador
     * @param dni Dni del administrador
     * @param direccion Dirección del administrador
     * @param puesto Puesto del administrador
     * @param contrasena Contraseña del administrador
     * @param nombre_departamento  Nombre del departamento en el que trabaja el administrador
     * @param fechaAlta fecha de alta del administrador
     */
    public Administrador(String nombre, String apellidos, String email, String telefono, String dni, String direccion, String puesto, String contrasena, String nombre_departamento, LocalDate fechaAlta) {
        super(nombre, apellidos, email, telefono, dni, direccion, puesto, contrasena, nombre_departamento, fechaAlta);
        Encriptar en = new Encriptar();
        try {
            this.setPermisos(en.encriptar("153"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor vacio de la clase Administrador
     */
    public Administrador() {
    }

    /**
     * Metodo que devuelve el Administrador pasado a String
     *
     * @return String con los atributos
     */
    @Override
    public String toString() {
        return "Administrador {" +
                "id-> " + this.getId() +
                ", nombre-> " + this.getNombre() +
                ", apellidos-> " + this.getApellidos() +
                ", email='" + this.getEmail() +
                ", telefono-> " + this.getTelefono() +
                ", dni-> " + this.getDni() +
                ", direccion-> " + this.getDireccion() +
                ", puesto-> " + this.getPuesto() +
                ", permisos-> " + this.getPermisos() +
                ", nombre_departamento-> " + this.getNombre_departamento() +
                ", fechaAlta=" + this.getFechaAlta() +
                '}';
    }
}
