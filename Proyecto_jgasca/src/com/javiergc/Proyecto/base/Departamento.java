package com.javiergc.Proyecto.base;

import org.bson.types.ObjectId;

/**
 * Clase Departamento
 */
public class Departamento {
    private ObjectId id;
    private String nombre;
    private int planta;
    private String reparacion;

    /**
     * Constructor de la calse Deparatametno
     * @param nombre Nombre el departamento
     * @param planta Planta en la que se situa
     * @param reparacion String que determina si el departamento corresponde a un depratamento de clientes o  de tecnicos.
     */
    public Departamento(String nombre, int planta, String reparacion) {
        this.nombre = nombre;
        this.planta = planta;
        this.reparacion = reparacion;
    }

    /**
     * Constructor vacio de la clase departamento
     */
    public Departamento(){}

    /**
     * Metodo que devuelve el nombre del departamento
     * @return Nombre del departamento
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre del departamento
     * @param nombre Nuevo nombre del departamento
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve la planta del departameto
     * @return Planta en la que se situa el departamento
     */
    public int getPlanta() {
        return planta;
    }

    /**
     * Metododo que estable la planta del departamento
     * @param planta Nueva plata en la que se situa el departamento
     */
    public void setPlanta(int planta) {
        this.planta = planta;
    }

    /**
     * Metodo que devuelve si el departamento corresponde a tecnicos de reparación o no
     * @return si o no String
     */
    public String getReparacion() {
        return reparacion;
    }

    /**
     * Metodo que establece si el departamento es de reparación
     * @param reparacion si o no
     */
    public void setReparacion(String reparacion) {
        this.reparacion = reparacion;
    }

    /**
     * Metodo que devuelve el id del departamento
     * @return id del departamento
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Metodo que establece el id del departamento
     * @param id nuevo id del departamento
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el nombre del departamento
     *
     * @return String con el nombre del departamento
     */
    @Override
    public String toString() {
        return nombre;
    }
}
