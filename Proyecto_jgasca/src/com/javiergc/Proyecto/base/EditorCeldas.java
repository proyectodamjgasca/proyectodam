package com.javiergc.Proyecto.base;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Clase EditorCeldas que cambia el color de la celda segun el estado de la incidencia que contiene
 */
public class EditorCeldas extends JLabel implements TableCellRenderer{
    int Row,Columns;

    /**
     * Metodo que establece la fila
     * @param Row Fila
     */
    public void setRow(int Row){
        this.Row=Row;
    }

    /**
     * Metodo que establece la columna que se querra cambiar
     * @param Columns Columna a cambiar
     */
    public void setColumns(int Columns){
        this.Columns=Columns;
    }

    /**
     * Permite que se vea el color de la celda
     */
    public EditorCeldas() {
        setOpaque(true);
    }

    /**
     * Returns the component used for drawing the cell.  This method is
     * used to configure the renderer appropriately before drawing.
     * <p>
     * The <code>TableCellRenderer</code> is also responsible for rendering the
     * the cell representing the table's current DnD drop location if
     * it has one. If this renderer cares about rendering
     * the DnD drop location, it should query the table directly to
     * see if the given row and column represent the drop location:
     * <pre>
     *     JTable.DropLocation dropLocation = table.getDropLocation();
     *     if (dropLocation != null
     *             &amp;&amp; !dropLocation.isInsertRow()
     *             &amp;&amp; !dropLocation.isInsertColumn()
     *             &amp;&amp; dropLocation.getRow() == row
     *             &amp;&amp; dropLocation.getColumn() == column) {
     *
     *         // this cell represents the current drop location
     *         // so render it specially, perhaps with a different color
     *     }
     * </pre>
     * <p>
     * During a printing operation, this method will be called with
     * <code>isSelected</code> and <code>hasFocus</code> values of
     * <code>false</code> to prevent selection and focus from appearing
     * in the printed output. To do other customization based on whether
     * or not the table is being printed, check the return value from
     * {@link javax.swing.JComponent#isPaintingForPrint()}.
     *
     * @param   table           the <code>JTable</code> that is asking the
     *                          renderer to draw; can be <code>null</code>
     * @param   value           the value of the cell to be rendered.  It is
     *                          up to the specific renderer to interpret
     *                          and draw the value.  For example, if
     *                          <code>value</code>
     *                          is the string "true", it could be rendered as a
     *                          string or it could be rendered as a check
     *                          box that is checked.  <code>null</code> is a
     *                          valid value
     * @param   isSelected      true if the cell is to be rendered with the
     *                          selection highlighted; otherwise false
     * @param   hasFocus        if true, render cell appropriately.  For
     *                          example, put a special border on the cell, if
     *                          the cell can be edited, render in the color used
     *                          to indicate editing
     * @param   row             the row index of the cell being drawn.  When
     *                          drawing the header, the value of
     *                          <code>row</code> is -1
     * @param   column          the column index of the cell being drawn
     * @see javax.swing.JComponent#isPaintingForPrint()
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (column == Columns){
            switch ((String)table.getValueAt(row,6)){
                case "Pendiente":
                    setBackground(Color.gray);
                    setForeground(Color.black);
                    setText("Pendiente");
                    break;
                case "Procesando":
                    setBackground(Color.yellow);
                    setForeground(Color.BLACK);
                    setText("Procesando");
                    break;
                case "Solucionado":
                    setBackground(Color.green);
                    setForeground(Color.BLACK);
                    setText("Solucionado");
                    break;
            }
        }
        return this;
    }
}

