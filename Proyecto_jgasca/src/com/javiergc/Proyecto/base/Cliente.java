package com.javiergc.Proyecto.base;

import Encriptacion.Encriptar;

import java.time.LocalDate;

/**
 * Clase Cliente
 */
public class Cliente extends Usuario {

    /**
     * Constructor de la clase cliente
     *
     * @param nombre Nombre del cliente
     * @param apellidos Apellidos del cliente
     * @param email Email del cliente
     * @param telefono Telefono del cliente
     * @param dni Dni del cliente
     * @param direccion Dirección del cliente
     *  @param puesto Puesto del cliente
     * @param contrasena Contraseña del cliente
     * @param nombre_departamento Nombre del departamento al que pertenece el cliente
     * @param fechaAlta fecha de alta del cliente
     */
    public Cliente(String nombre, String apellidos, String email, String telefono, String dni, String direccion, String puesto, String contrasena, String nombre_departamento, LocalDate fechaAlta) {
        super(nombre, apellidos, email, telefono, dni, direccion, puesto, contrasena, nombre_departamento, fechaAlta);
        Encriptar en = new Encriptar();
        try {
            this.setPermisos(en.encriptar("25"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor vacio de la clase Cliente
     */
    public Cliente(){}

    /**
     * Metodo que devuelve el Cliente pasado a String
     *
     * @return String con los atributos
     */
    @Override
    public String toString() {
        return "Cliente{" +
                "id-> " + this.getId() +
                ", nombre-> " + this.getNombre() +
                ", apellidos-> " + this.getApellidos() +
                ", email-> " + this.getEmail() +
                ", telefono-> " + this.getTelefono() +
                ", dni-> " + this.getDni() +
                ", direccion-> " + this.getDireccion() +
                ", puesto-> " + this.getPuesto() +
                ", nombre_departamento-> " + this.getNombre_departamento() +
                ", fechaAlta-> " + this.getFechaAlta() +
                '}';
    }
}
