package com.javiergc.Proyecto.main;

import com.javiergc.Proyecto.gui.controladores.ControladorLogin;
import com.javiergc.Proyecto.gui.Modelo;
import com.javiergc.Proyecto.gui.Login;

/**
 * Clase Main
 */
public class main {
    /**
     * Metodo main que inicia el programa
     * @param args args
     */
    public  static void main(String[] args){new ControladorLogin(new Modelo(),new Login());}
}
