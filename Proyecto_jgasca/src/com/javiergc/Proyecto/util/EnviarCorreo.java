package com.javiergc.Proyecto.util;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Clase EnviarCorreo
 */
public class EnviarCorreo extends Thread{
    private String asunto;
    private String destinatario;
    private String cuerpo;

    /**
     * Constructor de la clase EnviarCorreo
     * @param destinatario Correo del destinatario
     * @param asunto Asunto del correo
     * @param cuerpo Cuerpo del correo
     */
    public EnviarCorreo(String asunto, String cuerpo, String destinatario){
        this.asunto = asunto;
        this.cuerpo = cuerpo;
        this.destinatario = destinatario;
    }

    /**
     * Envio del correo
     */
    @Override
    public void run() {
        String remitente = "incidenciaselcorteingles";

        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", "Montessori2021");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            message.setSubject(asunto);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, "Montessori2021");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }
}
