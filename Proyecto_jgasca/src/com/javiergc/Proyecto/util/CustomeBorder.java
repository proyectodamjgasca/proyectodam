package com.javiergc.Proyecto.util;

import javax.swing.*;
import javax.swing.border.AbstractBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Clase CustomeBorder que redondea los bordes de los JTextField
 */
public class CustomeBorder extends AbstractBorder {
    /**
     * Metodo que pinta los bordes
     */
    @Override
    public void paintBorder(Component c, Graphics g, int x, int y,
                            int width, int height) {
        // TODO Auto-generated method stubs
        super.paintBorder(c, g, x, y, width, height);
        Graphics2D g2d = (Graphics2D)g;
        g2d.setStroke(new BasicStroke(12));
        g2d.setColor(Color.black);
        g2d.drawRoundRect(x, y, width - 1, height + 3, 25, 25);
    }

    /**
     * Metodo que establece cuanto se redondean los bordes
     * @param txt JTextField al que queremos redondear los bordes
     */
    public static void redondearBordesTextField(JTextField txt){
        txt.setBorder(BorderFactory.createCompoundBorder(
                new CustomeBorder(),
                new EmptyBorder(new Insets(5, 10, 5, 10))));
    }
}


