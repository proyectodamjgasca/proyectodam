package com.javiergc.Proyecto.gui.controladores;

import com.javiergc.Proyecto.gui.Login;
import com.javiergc.Proyecto.gui.Modelo;
import com.javiergc.Proyecto.gui.RegistroUsuariosAdmin;
import com.javiergc.Proyecto.gui.VistaPanelAdmin;
import net.sf.jasperreports.engine.JRException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase ControladorPanelAdmin
 */
public class ControladorPanelAdmin implements ActionListener {
    Modelo modelo;
    VistaPanelAdmin vista;

    /**
     * Constructor de la clase ControladorPanelAdmin
     * @param modelo Modelo del programa
     */
    public ControladorPanelAdmin(Modelo modelo){
        this.modelo = modelo;
        vista = new VistaPanelAdmin();
        addActionListeners(this);
    }

    /**
     * Metodo que añade los actionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnIncidencias.addActionListener(listener);
        vista.btnRegistrarUsuarios.addActionListener(listener);
        vista.btnRegistroDepartamentos.addActionListener(listener);
        vista.btnCerrarSesion.addActionListener(listener);
        vista.btnDescargarIncidencias.addActionListener(listener);
        vista.btnDescargarUsuarios.addActionListener(listener);
    }

    /**
     * Metodo que se ejecuta cada vez que presionamos un boton y segun el boton presionado ejecuta distinto codigo
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "registroUsuarios":
                ControladorRegistroUsu usu = new ControladorRegistroUsu(modelo);
                vista.frame.dispose();
                break;
            case "registroDepartamentos":
                ControladorDepartamentos depar = new ControladorDepartamentos(modelo);
                vista.frame.dispose();
                break;
            case "incidencias":
                ControladorListaIncidencias inci = new ControladorListaIncidencias(modelo);
                vista.frame.dispose();
                break;
            case "cerrarSesion":
                modelo.desconectar();
                modelo = null;
                vista.frame.dispose();
                ControladorLogin login = new ControladorLogin(new Modelo(), new Login());
                break;
            case "descargarIncidencias":
                try {
                    modelo.writeExcel();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case "descargarUsuarios":
                    modelo.generarPdf();
                break;
        }
    }
}
