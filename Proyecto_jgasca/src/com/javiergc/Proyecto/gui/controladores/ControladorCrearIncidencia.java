package com.javiergc.Proyecto.gui.controladores;

import com.javiergc.Proyecto.base.Incidencia;
import com.javiergc.Proyecto.gui.Modelo;
import com.javiergc.Proyecto.gui.VistaCrearIncidencia;
import com.javiergc.Proyecto.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.time.LocalDate;

/**
 * Clase ControladorCrearIncidencia
 */
public class ControladorCrearIncidencia implements ActionListener, KeyListener, ListSelectionListener, WindowListener {
    Modelo modelo;
    VistaCrearIncidencia vista;
    ControladorListaIncidencias listaInci;

    /**
     * Controlador de la clase CrearIncidencia
     * @param modelo Modelo del programa
     * @param listaInci Controlado de la listaIncidencias
     */
    public ControladorCrearIncidencia(Modelo modelo, ControladorListaIncidencias listaInci){
       this.modelo = modelo;
       this.listaInci = listaInci;
       vista = new VistaCrearIncidencia();
       addActionListeners(this);
       vista.frame.addWindowListener(this);
       rellenarCombo();
       vista.txtUsuario.setText(modelo.getUser().getNombre() + " " + modelo.getUser().getApellidos());
    }

    /**
     * Metodo que añade los actionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnCrear.addActionListener(listener);
    }

    /**
     * Metodo que ejecuta las acciones correspondientes al pulsar un boton
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "crear":
                Incidencia inci = new Incidencia();
                if (!vista.txtAsunto.getText().equals("") && !vista.txtAreaDescripcion.getText().equals("") && vista.comboBox1.getSelectedItem() != null &&!vista.comboBox1.getSelectedItem().toString().equals("") ){
                    inci.setId_cliente(modelo.getUser().getId());
                    inci.setAsunto(vista.txtAsunto.getText());
                    inci.setNombre_departamento_reparacion(vista.comboBox1.getSelectedItem().toString());
                    inci.setDescripcion(vista.txtAreaDescripcion.getText());
                    inci.setFecha_apertura(LocalDate.now());
                    inci.setEstado("Pendiente");
                    inci.setNombre_departamento(modelo.getUser().getNombre_departamento());
                    modelo.guardarObjeto(inci);
                    listaInci.cargarIncidenciasDepartamento();
                    vista.frame.setVisible(false);
                }else {
                    Util.showWarningAlert("Faltan campos por rellenar");
                }

                break;
        }
    }

    /**
     * Metodo que rellena el combobox con los departamentos
     */
    public void rellenarCombo(){
        vista.comboBox1.removeAllItems();
        modelo.setComboBoxDepartamento(modelo.getDepartamentosReparacion(),vista.comboBox1);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Metodo que cierra solo la ventana que crea la incidecnia
     * @param e WindowsEvent
     */
    @Override
    public void windowClosing(WindowEvent e) {
        vista.frame.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
