package com.javiergc.Proyecto.gui.controladores;

import com.javiergc.Proyecto.base.Departamento;
import com.javiergc.Proyecto.gui.Modelo;
import com.javiergc.Proyecto.gui.VistaDepartamento;
import com.javiergc.Proyecto.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Clase ControladorDepartamentos
 */
public class ControladorDepartamentos implements ActionListener, KeyListener, ListSelectionListener , WindowListener{
    private Modelo modelo;
    private VistaDepartamento vista;
    private Departamento depaSeleccinado;
    private Thread hiloRecargarDepartamentos;
    private boolean hiloActivo;

    /**
     * Constructor de la clase ControladorDepartamentos
     * @param modelo Modelo del prgrama
     */
    public ControladorDepartamentos(Modelo modelo) {
        this.modelo = modelo;
        this.vista = new VistaDepartamento();
        this.hiloActivo = true;
        addActionListeners(this);
        addKeyListeners(this);
        addActionListenerTable();
        vista.frame.addWindowListener(this);
        iniciarTabla();

        cargarFilas(modelo.getDepartamentos());

        recargarDepartamentoAuto();

    }

    /**
     * Metodo que recarga los deparatamento cada ciertos segundos
     */
    private void recargarDepartamentoAuto() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (hiloActivo) {
                    try {
                        Thread.sleep(60000);
                        cargarFilas(modelo.getDepartamentos());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        hiloRecargarDepartamentos = new Thread(runnable);
        hiloRecargarDepartamentos.start();
    }


    /**
     * Metodo que añade los actionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnNuevo.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.btnAtras.addActionListener(listener);
    }

    /**
     * Metodo que añade los KeyListener a el campo de texto de buscar
     * @param listener KeyListener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscar.addKeyListener(listener);
    }

    /**
     * Metodo que añade los actionListener a los botones
     * @param e ActionListener
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "crear":
                    if (!vista.txtNombre.getText().equals("") && !vista.txtPiso.getText().equals("")){
                        if (modelo.isNumeric(vista.txtPiso.getText())){
                            if (!comprobarNombre(vista.txtNombre.getText())){
                                Departamento depa = new Departamento(vista.txtNombre.getText(),Integer.parseInt(vista.txtPiso.getText()), vista.comboReparacion.getSelectedItem().toString());
                                modelo.guardarObjeto(depa);
                                limpiarCamposDepartamento();
                                cargarFilas(modelo.getDepartamentos());
                            }else {
                                Util.showErrorAlert("El nombre del departamento ya existe");
                            }
                        }else {
                            Util.showWarningAlert("La planta tiene que ser numerico");
                        }
                    }else {
                        Util.showWarningAlert("No has competado todos los campos");
                    }
                break;
            case "eliminar":
                if (vista.listaDepartamentos.getSelectedRow() != -1){
                    int filaBorrar = vista.listaDepartamentos.getSelectedRow();
                    String nombreBorrar = (String) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.borrarDepartamento(nombreBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    limpiarCamposDepartamento();
                    cargarFilas(modelo.getDepartamentos());
                }else {
                    Util.showErrorAlert("No has seleccionado ningun departamento");
                }

                break;
            case "modificar":
                if (vista.listaDepartamentos.getSelectedRow() != -1){
                    if (!vista.txtNombre.getText().equals("") && !vista.txtPiso.getText().equals("")){
                        if (depaSeleccinado.getNombre().equals(vista.txtNombre.getText())){
                            if (modelo.isNumeric(vista.txtPiso.getText())) {
                                depaSeleccinado.setPlanta(Integer.parseInt(vista.txtPiso.getText()));
                                depaSeleccinado.setReparacion(vista.comboReparacion.getSelectedItem().toString());
                                modelo.modificarDepartamento(depaSeleccinado);
                            }else {
                                Util.showWarningAlert("Planta debe de ser numerico");
                            }
                        }else {
                            if (modelo.isNumeric(vista.txtPiso.getText())) {
                                if (!comprobarNombre(vista.txtNombre.getText())) {
                                    modelo.modificarNombreDepartamentoClientesTecnicos(depaSeleccinado.getNombre(),vista.txtNombre.getText());
                                    depaSeleccinado.setNombre(vista.txtNombre.getText());
                                    depaSeleccinado.setPlanta(Integer.parseInt(vista.txtPiso.getText()));
                                    depaSeleccinado.setReparacion(vista.comboReparacion.getSelectedItem().toString());
                                    modelo.modificarDepartamento(depaSeleccinado);
                                }else {
                                    Util.showWarningAlert("El nombre del departamento ya existe");
                                }
                            }else {
                                Util.showWarningAlert("Planta debe de ser numerico");
                            }
                        }
                    }else {
                        Util.showWarningAlert("Hay campos vacios");
                    }
                    limpiarCamposDepartamento();
                }else {
                    Util.showErrorAlert("No has seleccionado ningun departamento");
                }

                break;

            case "atras":
            ControladorPanelAdmin admin = new ControladorPanelAdmin(modelo);
            hiloActivo = false;
            vista.frame.dispose();
                break;
        }
        cargarFilas(modelo.getDepartamentos());
    }

    /**
     * Metodo que comprueba que el nombre del nuevo departamento no coincida con uno existente
     * @param nombre Nombre del nuevo departamento a comprobar
     * @return true o false
     */
    private boolean comprobarNombre(String nombre) {
        for (Departamento dp:modelo.getDepartamentos()) {
            if (dp.getNombre().toLowerCase().equals(nombre.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que esta a la escucha de cuando se selecciona una fila del JTable
     */
    private void addActionListenerTable(){
        vista.listaDepartamentos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vista.listaDepartamentos.getSelectedRow();
                vista.txtNombre.setText((String) vista.listaDepartamentos.getValueAt(row,0));
                vista.txtPiso.setText(vista.listaDepartamentos.getValueAt(row,1) + "");
                vista.comboReparacion.setSelectedItem(vista.listaDepartamentos.getValueAt(row,2));
                depaSeleccinado = modelo.buscarDepartamentoPorNombre(vista.listaDepartamentos.getValueAt(row,0).toString());
            }
        });
    }

    /**
     * Metodo que establece las cabeceras de la tabla
     */
    private void iniciarTabla(){
        String[] headers = {"Nombre", "Planta", "Reparación"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Metodo que limpia todos los campos
     */
    private void limpiarCamposDepartamento() {
        vista.txtNombre.setText("");
        vista.txtPiso.setText("");
        vista.comboReparacion.setSelectedIndex(0);
    }

    /**
     * Metodo que carga los datos recibios en un JTable
     * @param lista Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    private void cargarFilas(ArrayList<Departamento> lista) {
        Object[] fila = new Object[3];
        vista.dtm.setRowCount(0);

        for (Departamento departamento: lista) {
            fila[0]=departamento.getNombre();
            fila[1]=departamento.getPlanta();
            fila[2]=departamento.getReparacion();

            vista.dtm.addRow(fila);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Metodo que se ejecuta cuando se presiona una tecla en el txtBuscar
     * @param e KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscar) {
            cargarFilas(modelo.getDepartamentos(vista.txtBuscar.getText()));
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Metodo que detecta cuando la ventana se esta cerrando
     * @param e WindowEvent
     */
    @Override
    public void windowClosing(WindowEvent e) {
        if (JOptionPane.showConfirmDialog(null, "¿Desea salir de la aplicación?", "Gestion departamentos", JOptionPane.YES_OPTION) == JOptionPane.YES_OPTION) {
            vista.frame.dispose();
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
