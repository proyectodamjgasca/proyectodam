package com.javiergc.Proyecto.gui;

import Encriptacion.Encriptar;
import com.javiergc.Proyecto.base.*;
import com.javiergc.Proyecto.util.GenerarPdfUsuarios;
import com.javiergc.Proyecto.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase Modelo
 */
public class Modelo {
    private MongoClient mc;
    private MongoCollection<Document> clientes;
    private MongoCollection<Document> tecnicos;
    private MongoCollection<Document> incidencias;
    private MongoCollection<Document> departamentos;

    private Usuario user;

    /**
     * Metodo conectar que conecta con la base de datos
     */
    public void conectar() {
        mc = new MongoClient();

        String DATABASE = "Incidencias";
        MongoDatabase db = mc.getDatabase(DATABASE);

        String COLECCION_TECNICOS = "Tecnico";
        tecnicos = db.getCollection(COLECCION_TECNICOS);
        String COLECCION_INCIDENCIAS = "Incidencia";
        incidencias = db.getCollection(COLECCION_INCIDENCIAS);
        String COLECCION_CLIENTES = "Cliente";
        clientes = db.getCollection(COLECCION_CLIENTES);
        String COLECCION_DEPARTAMENTOS = "Departamentos";
        departamentos = db.getCollection(COLECCION_DEPARTAMENTOS);

        boolean adminExist = false;

        for (Tecnico tecnico:getTecnicos()) {
            try {
                if (Encriptar.desencriptar(tecnico.getPermisos()).equals("153")){
                    adminExist = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (getTecnicos().size() == 0 || adminExist == false){
            if (buscarDepartamentoPorNombre("Administración") == null){
                Departamento departamento = new Departamento();
                departamento.setNombre("Administración");
                departamento.setPlanta(-2);
                departamento.setReparacion("si");
                guardarObjeto(departamento);
            }

            Administrador admin = new Administrador();
            admin.setNombre("Administrador");
            admin.setApellidos("admin");
            admin.setEmail("admin@gmail.com");
            admin.setDni("1234");
            admin.setTelefono("0");
            admin.setDireccion("Ninguna");
            admin.setPermisosCifrado("153");
            admin.setContrasenaHaseada("1234");
            admin.setNombre_departamento("Administración");
            admin.setFechaAlta(LocalDate.now());
            guardarObjeto(admin);
        }
    }

    /**
     * Metodo que desconecta de la base de datos
     */
    public void desconectar() {
        mc.close();
        mc = null;
    }

    /**
     * Metodo que devuelve el MongoClient
     *
     * @return MongoClient
     */
    public MongoClient getCliente() {
        return mc;
    }

    /**
     * Metodo que guarda en la base de datos cualquier objeto
     *
     * @param obj Objeto de la clase Tecnico, Cliente, Incidencia o Deparatamento
     */
    public void guardarObjeto(Object obj) {
        if (obj instanceof Tecnico || obj instanceof Administrador) {
            tecnicos.insertOne(objectToDocument(obj));
        } else if (obj instanceof Cliente) {
            clientes.insertOne(objectToDocument(obj));
        } else if (obj instanceof Incidencia) {
            incidencias.insertOne(objectToDocument(obj));
        } else if (obj instanceof Departamento) {
            departamentos.insertOne(objectToDocument(obj));
        }
    }

    /**
     * Metodo que pasa un objeto de las clase Tecnico, Cliente, Incidencia o Departamento a un Document
     *
     * @param obj Objeto de la clase Tecnico, Cliente, Incidencia o Departamento
     * @return Devuelve un documento
     */
    public Document objectToDocument(Object obj) {
        Document dc = new Document();

        if (obj instanceof Tecnico) {
            Tecnico tecnico = (Tecnico) obj;

            dc.append("nombre", tecnico.getNombre());
            dc.append("apellidos", tecnico.getApellidos());
            dc.append("email", tecnico.getEmail());
            dc.append("telefono", tecnico.getTelefono());
            dc.append("dni", tecnico.getDni());
            dc.append("direccion", tecnico.getDireccion());
            dc.append("puesto", tecnico.getPuesto());
            dc.append("contrasena", tecnico.getContrasena());
            dc.append("nombre_departamento", tecnico.getNombre_departamento());
            dc.append("permisos", tecnico.getPermisos());
            dc.append("fechaAlta", Util.formatearFecha(tecnico.getFechaAlta()));
        } else if (obj instanceof Incidencia) {
            Incidencia incidencia = (Incidencia) obj;

            dc.append("asunto", incidencia.getAsunto());
            dc.append("solucion", incidencia.getSolucion());
            dc.append("anotaciones", incidencia.getAnotaciones());
            dc.append("estado", incidencia.getEstado());
            dc.append("descripcion", incidencia.getDescripcion());
            dc.append("fecha_apertura", Util.formatearFecha(incidencia.getFecha_apertura()));
            if (incidencia.getFecha_cierre() != null){
                dc.append("fecha_cierre", Util.formatearFecha(incidencia.getFecha_cierre()));
            }else {
                dc.append("fecha_cierre", "null");
            }
            dc.append("id_tecnico", incidencia.getId_tecnico());
            dc.append("id_cliente", incidencia.getId_cliente());
            dc.append("nombre_departamento", incidencia.getNombre_departamento());
            dc.append("nombre_departamento_reparacion", incidencia.getNombre_departamento_reparacion());
        } else if (obj instanceof Cliente) {
            Cliente cliente = (Cliente) obj;

            dc.append("nombre", cliente.getNombre());
            dc.append("apellidos", cliente.getApellidos());
            dc.append("email", cliente.getEmail());
            dc.append("telefono", cliente.getTelefono());
            dc.append("dni", cliente.getDni());
            dc.append("direccion", cliente.getDireccion());
            dc.append("puesto", cliente.getPuesto());
            dc.append("contrasena", cliente.getContrasena());
            dc.append("nombre_departamento", cliente.getNombre_departamento());
            dc.append("permisos", cliente.getPermisos());
            dc.append("fechaAlta", Util.formatearFecha(cliente.getFechaAlta()));
        } else if (obj instanceof Departamento) {
            Departamento departamento = (Departamento) obj;

            dc.append("nombre", departamento.getNombre());
            dc.append("planta", departamento.getPlanta());
            dc.append("reparacion", departamento.getReparacion());
        } else if (obj instanceof Administrador) {
            Administrador admin = (Administrador) obj;

            dc.append("nombre", admin.getNombre());
            dc.append("apellidos", admin.getApellidos());
            dc.append("email", admin.getEmail());
            dc.append("telefono", admin.getTelefono());
            dc.append("dni", admin.getDni());
            dc.append("direccion", admin.getDireccion());
            dc.append("puesto", admin.getPuesto());
            dc.append("contrasena", admin.getContrasena());
            dc.append("nombre_departamento", admin.getNombre_departamento());
            dc.append("permisos", admin.getPermisos());
            dc.append("fechaAlta", Util.formatearFecha(admin.getFechaAlta()));
        } else {
            return null;
        }
        return dc;
    }

    /**
     * Metodo que pasa un docuemtno a un objeto de la clase Cliente
     *
     * @param dc Documento que transformar
     * @return Objeto de la clase Cliente
     */
    public Cliente documentToCliente(Document dc) {
        Cliente cliente = new Cliente();

        cliente.setId(dc.getObjectId("_id"));
        cliente.setNombre(dc.getString("nombre"));
        cliente.setApellidos(dc.getString("apellidos"));
        cliente.setEmail(dc.getString("email"));
        cliente.setTelefono(dc.getString("telefono"));
        cliente.setDni(dc.getString("dni"));
        cliente.setDireccion(dc.getString("direccion"));
        cliente.setNombre_departamento(dc.getString("nombre_departamento"));
        cliente.setPermisos(dc.getString("permisos"));
        cliente.setPuesto(dc.getString("puesto"));
        cliente.setContrasena(dc.getString("contrasena"));
        cliente.setFechaAlta(Util.parsearFecha(dc.getString("fechaAlta")));
        return cliente;
    }

    /**
     * Metodo que pasa un docuemtno a un objeto de la clase Tecnico
     *
     * @param dc Documento que transformar
     * @return Objeto de la clase Cliente
     */
    public Tecnico documentToTecnico(Document dc) {
        Tecnico tecnico = new Tecnico();

        tecnico.setId(dc.getObjectId("_id"));
        tecnico.setNombre(dc.getString("nombre"));
        tecnico.setApellidos(dc.getString("apellidos"));
        tecnico.setEmail(dc.getString("email"));
        tecnico.setTelefono(dc.getString("telefono"));
        tecnico.setDni(dc.getString("dni"));
        tecnico.setDireccion(dc.getString("direccion"));
        tecnico.setNombre_departamento(dc.getString("nombre_departamento"));
        tecnico.setPermisos(dc.getString("permisos"));
        tecnico.setPuesto(dc.getString("puesto"));
        tecnico.setContrasena(dc.getString("contrasena"));
        tecnico.setFechaAlta(Util.parsearFecha(dc.getString("fechaAlta")));
        return tecnico;
    }

    /**
     * Metodo que pasa un docuemtno a un objeto de la clase Tecnico
     *
     * @param dc Documento que transformar
     * @return Objeto de la clase Cliente
     */
    public Administrador documentToAdministrador(Document dc) {
        Administrador admin = new Administrador();

        admin.setId(dc.getObjectId("_id"));
        admin.setNombre(dc.getString("nombre"));
        admin.setApellidos(dc.getString("apellidos"));
        admin.setEmail(dc.getString("email"));
        admin.setTelefono(dc.getString("telefono"));
        admin.setDni(dc.getString("dni"));
        admin.setDireccion(dc.getString("direccion"));
        admin.setNombre_departamento(dc.getString("nombre_departamento"));
        admin.setPermisos(dc.getString("permisos"));
        admin.setPuesto(dc.getString("puesto"));
        admin.setContrasena(dc.getString("contrasena"));
        admin.setFechaAlta(Util.parsearFecha(dc.getString("fechaAlta")));
        return admin;
    }

    /**
     * Metodo que pasa un documento a un objeto de la clase Incidencia
     *
     * @param dc Documento que transformar
     * @return Objeto de la clase Incidencia
     */
    public Incidencia documentToIncidencia(Document dc) {
        Incidencia incidencia = new Incidencia();

        incidencia.setId(dc.getObjectId("_id"));
        incidencia.setAsunto(dc.getString("asunto"));
        incidencia.setSolucion(dc.getString("solucion"));
        incidencia.setAnotaciones(dc.getString("anotaciones"));
        incidencia.setEstado(dc.getString("estado"));
        incidencia.setDescripcion(dc.getString("descripcion"));
        incidencia.setFecha_apertura(Util.parsearFecha(dc.getString("fecha_apertura")));
        if (!dc.getString("fecha_cierre").equals("null")){
            incidencia.setFecha_cierre(Util.parsearFecha(dc.getString("fecha_cierre")));
        }else {
            incidencia.setFecha_cierre(null);
        }
        incidencia.setId_tecnico(dc.getObjectId("id_tecnico"));
        incidencia.setId_cliente(dc.getObjectId("id_cliente"));
        incidencia.setNombre_departamento(dc.getString("nombre_departamento"));
        incidencia.setNombre_departamento_reparacion(dc.getString("nombre_departamento_reparacion"));

        return incidencia;
    }

    /**
     * Metodo que pasa un documento a un objeto de la clase Departamento
     *
     * @param dc Documento que transformar
     * @return Objeto de la clase Departamento
     */
    public Departamento documentToDepartamento(Document dc) {
        Departamento departamento = new Departamento();

        departamento.setId(dc.getObjectId("_id"));
        departamento.setNombre(dc.getString("nombre"));
        departamento.setPlanta(dc.getInteger("planta"));
        departamento.setReparacion(dc.getString("reparacion"));

        return departamento;
    }

    /**
     * Metodo que devulve un arraylist con todos las incidencias
     *
     * @return Devuelve un arraylist con todos las incidencias
     */
    public ArrayList<Incidencia> getIncidencias() {
        ArrayList<Incidencia> lista = new ArrayList<>();

        for (Document document : incidencias.find()) {
            lista.add(documentToIncidencia(document));
        }
        Collections.sort(lista, new Comparator<Incidencia>() {
            public int compare(Incidencia o1, Incidencia o2) {
                if (o1.getFecha_apertura() == null || o2.getFecha_apertura() == null)
                    return 0;
                return o1.getFecha_apertura().compareTo(o2.getFecha_apertura());
            }
        });
        Collections.reverse(lista);
        return lista;
    }

    /**
     * Metodo que devulve un arraylist con todos las incidencias comparando entre id, nombre cliente, nombre tecnico.
     * @param comparador String cadena a comparar con la base de datos
     * @return Devuelve un arraylist con todos las incidencias
     */
    public ArrayList<Incidencia> getIncidencias(String comparador) {
        ArrayList<Incidencia> lista = new ArrayList<>();

        for (Document document : incidencias.find()) {
            lista.add(documentToIncidencia(document));
        }
        Collections.sort(lista, new Comparator<Incidencia>() {
            public int compare(Incidencia o1, Incidencia o2) {
                if (o1.getFecha_apertura() == null || o2.getFecha_apertura() == null)
                    return 0;
                return o1.getFecha_apertura().compareTo(o2.getFecha_apertura());
            }
        });
        Collections.reverse(lista);
        return lista;
    }


    /**
     * Metodo que devulve un arraylist con todos los clientes
     *
     * @return Devuelve un arraylist con todos los clientes
     */
    public ArrayList<Cliente> getClientes() {
        ArrayList<Cliente> lista = new ArrayList<>();

        for (Document document : clientes.find()) {
            lista.add(documentToCliente(document));
        }

        Collections.sort(lista, new Comparator<Cliente>() {
            @Override
            public int compare(Cliente p1, Cliente p2) {
                return new String(p1.getNombre()).compareTo(new String(p2.getNombre()));
            }
        });
        return lista;
    }

    /**
     * Metodo que devulve un arraylist con todos los tecnicos
     *
     * @return Devuelve un arraylist con todos los tecnicos
     */
    public ArrayList<Tecnico> getTecnicos() {
        ArrayList<Tecnico> lista = new ArrayList<>();

        for (Document document : tecnicos.find()) {
            lista.add(documentToTecnico(document));
        }
        Collections.sort(lista, new Comparator<Tecnico>() {
            @Override
            public int compare(Tecnico p1, Tecnico p2) {
                return new String(p1.getNombre()).compareTo(new String(p2.getNombre()));
            }
        });
        return lista;
    }

    /**
     * Metodo que devulve un arraylist con todos los departamentos
     *
     * @return Devuelve un arraylist con todos los departamentos
     */
    public ArrayList<Departamento> getDepartamentos() {
        ArrayList<Departamento> lista = new ArrayList<>();

        for (Document document : departamentos.find()) {
            lista.add(documentToDepartamento(document));
        }
        Collections.sort(lista, new Comparator<Departamento>() {
            @Override
            public int compare(Departamento p1, Departamento p2) {
                return new String(p1.getNombre()).compareTo(new String(p2.getNombre()));
            }
        });
        return lista;
    }

    /**
     * Metodo que devulve un arraylist con todos los departamentos de reapración
     *
     * @return Devuelve un arraylist con todos los departamentos de reparación
     */
    public ArrayList<Departamento> getDepartamentosReparacion() {
        ArrayList<Departamento> lista = new ArrayList<>();
        for (Document document : departamentos.find()) {
            if (documentToDepartamento(document).getReparacion().equals("si")){
                lista.add(documentToDepartamento(document));
            }
        }
        Collections.sort(lista, new Comparator<Departamento>() {
            @Override
            public int compare(Departamento p1, Departamento p2) {
                return new String(p1.getNombre()).compareTo(new String(p2.getNombre()));
            }
        });
        return lista;
    }

    /**
     * Metodo que devulve un arraylist con todos los departamentos que no sean de reparación
     *
     * @return Devuelve un arraylist con todos los departamentos que no sean de reparación
     */
    public ArrayList<Departamento> getDepartamentosNoReparacion() {
        ArrayList<Departamento> lista = new ArrayList<>();
        for (Document document : departamentos.find()) {
            if (documentToDepartamento(document).getReparacion().equals("no")){
                lista.add(documentToDepartamento(document));
            }
        }
        Collections.sort(lista, new Comparator<Departamento>() {
            @Override
            public int compare(Departamento p1, Departamento p2) {
                return new String(p1.getNombre()).compareTo(new String(p2.getNombre()));
            }
        });
        return lista;
    }

    /**
     * Metodo que borrar el cliente
     *
     * @param cliente cliente a borrar
     */
    public void borrarCliente(Cliente cliente) {
        clientes.deleteOne(objectToDocument(cliente));
    }

    /**
     * Metodo que borrar el tecnico
     *
     * @param tecnico tecnico a borrar
     */
    public void borrarTecnico(Tecnico tecnico) {
        tecnicos.deleteOne(objectToDocument(tecnico));
    }

    /**
     * Metodo que borrar el administrador
     *
     * @param administrador administrador a borrar
     */
    public void borrarAdmin(Administrador administrador) {
        Util.showInformationAlert("No se puede eliminar los administradores");
        //tecnicos.deleteOne(objectToDocument(administrador));
    }

    /**
     * Metodo que borrar el incidencia
     *
     * @param incidencia incidencia a borrar
     */
    public void borrarIncidencia(Incidencia incidencia) {
        incidencias.deleteOne(objectToDocument(incidencia));
    }

    /**
     * Metodo que borrar el usuario por dni
     *
     * @param dni dni del usuario a borrar
     * @return true o false
     */
    public boolean borrarUsuarioPorDni(String dni) {
        ArrayList usu = buscarUsuarioPorDni(dni);
        if (usu != null){
            if (usu.get(0) instanceof Cliente){
                borrarCliente((Cliente) usu.get(0));
                return true;
            }
            if (usu.get(0) instanceof  Tecnico){
                Tecnico tec = (Tecnico) usu.get(0);
                borrarTecnico(tec);
                return true;
            }
            if (usu.get(0) instanceof  Administrador){
                Administrador tec = (Administrador) usu.get(0);
                borrarAdmin(tec);
                return true;
            }
        }

        return false;
    }

    /**
     * Metodo que busca un usuario por su nombre, apellidos o dni
     * @param comparador nombre, apellidos o dni del usuarios a buscar
     * @param incidencias True si se busca usuarios para el buscador de incidencias
     * @return Devuelve un arraylist con el usuarios
     */
    public ArrayList getUsuarios(String comparador, boolean incidencias){
        ArrayList lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        if (!incidencias){
            listaCriterios.add(new Document("dni", new Document("$regex", "/*" + comparador + "/*")));
            listaCriterios.add(new Document("email", new Document("$regex", "/*" + comparador + "/*")));
            listaCriterios.add(new Document("telefono", new Document("$regex", "/*" + comparador + "/*")));
            listaCriterios.add(new Document("direccion", new Document("$regex", "/*" + comparador + "/*")));
            listaCriterios.add(new Document("departamento", new Document("$regex", "/*" + comparador + "/*")));
        }


        query.append("$or", listaCriterios);

        return getArrayListUsuarios(lista, query);
    }

    /**
     * Metodo que busca un usuario por su nombre, apellidos o dni
     * @param comparador nombre, apellidos o dni del usuarios a buscar
     * @return Devuelve un arraylist con el usuarios
     */
    public ArrayList getDepartamentos(String comparador){
        ArrayList lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        if (isNumeric(comparador)){
            int comparador2 = Integer.parseInt(comparador);
            query.append("planta", comparador2);
        }else {
            listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
            query.append("$or", listaCriterios);
        }

        for (Document document : departamentos.find(query)) {
            lista.add(documentToDepartamento(document));
        }
        Collections.sort(lista, new Comparator<Departamento>() {
            @Override
            public int compare(Departamento p1, Departamento p2) {
                return new String(p1.getNombre()).compareTo(new String(p2.getNombre()));
            }
        });
        return lista;
    }

    /**
     * Metodo que obtiene la lista de usuarios atendiendo a unos criterios
     * @param lista lista donde seañadiran los usuarios
     * @param query cristerios de busqueda
     * @return Arraylist de usuarios
     */
    private ArrayList getArrayListUsuarios(ArrayList lista, Document query) {
        for (Document document : clientes.find(query)) {
            Cliente cliente = documentToCliente(document);
            lista.add(cliente);
            return lista;
        }

        for (Document document : tecnicos.find(query)) {
            Tecnico tecnico = documentToTecnico(document);
            try {
                if (Encriptar.desencriptar(tecnico.getPermisos()).equals("153")){
                    Administrador admin = documentToAdministrador(document);
                    lista.add(admin);
                    return lista;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            lista.add(tecnico);
            return lista;
        }

        return null;
    }

    /**
     * Metodo que busca un usuario por su dni
     * @param dni dni del usuario a buscar
     * @return Devuelve un arraylist con el usuario en la posicion 0 del array
     */
    public ArrayList buscarUsuarioPorDni(String dni){
        ArrayList lista = new ArrayList<>();
        Document query = new Document();

        query.append("dni", dni);

        return getArrayListUsuarios(lista, query);
    }

    /**
     * Metodo que busca id usuario por el id
     * @param id id del usuario a buscar
     * @return Devuelve un arraylist con el usuario en la posicion 0 del array
     */
    public ArrayList buscarUsuarioPorId(ObjectId id){
        ArrayList lista = new ArrayList<>();
        Document query = new Document();

        query.append("_id", id);

        return getArrayListUsuarios(lista, query);
    }

    /**
     * Metodo que busca las incidencias creadas por un usuario o por tecnico
     * @param id id del usuario
     * @return Devuelve un arraylist con las incidencias creadas por el usuario o por tecnico
     */
    public ArrayList<Incidencia> buscarIncidenciaPorIdCliente(ObjectId id){
        ArrayList lista = new ArrayList<>();
        Document query = new Document();

        query.append("id_cliente", id);

        for (Document document : incidencias.find(query)) {
            lista.add(documentToIncidencia(document));
        }

        if (lista.size() == 0){
            query.append("id_tecnico", id);

            for (Document document : incidencias.find(query)) {
                lista.add(documentToIncidencia(document));
            }
        }

        return lista;
    }

    /**
     * Metodo que busca id usuario si empieza por lo introducido
     * @param id parte del id de usuario a buscar
     * @param tecnicosSiNo Establece si en la busqueda se incluyen los tecnicos o no
     * @return Devuelve un arraylist con el usuario en la posicion
     */
    public ArrayList buscarUsuarioPorId2(String id, boolean tecnicosSiNo){
        Pattern pat = Pattern.compile("^"+ id +".*");

        ArrayList<Usuario> lista = new ArrayList<>();
        ArrayList<Usuario> lista2 = new ArrayList<Usuario>();

        for (Document document : clientes.find()) {
            Usuario cliente = documentToCliente(document);
            lista.add(cliente);
            lista2.add(cliente);
        }

        if (tecnicosSiNo){
            for (Document document : tecnicos.find()) {
                Usuario tecnico = documentToTecnico(document);
                lista.add(tecnico);
                lista2.add(tecnico);
            }
        }

        for (Usuario usuario: lista2) {
            Matcher mat = pat.matcher(usuario.getId().toString());
            if (!mat.matches()){
                lista.remove(usuario);
            }
        }
        return lista;
    }

    /**
     * Metodo que busca los usuarios parecidos a la cadena introducida y saca las incidencias a las que estan asociados
     * @param cadena Nombre o apellidos de las persona
     * @return Arraylist de Incidencias
     */
    public ArrayList<Incidencia> buscarIncidenciasUsuario(String cadena){
        ArrayList<Usuario> usuarios = getUsuarios(cadena, true);
        ArrayList<Incidencia> incidencias = new ArrayList<>();

        for (Usuario usu: usuarios) {
            for (Incidencia incidencia: buscarIncidenciaPorIdCliente(usu.getId())) {
                try {
                    if (Encriptar.desencriptar(user.getPermisos()).equals("41")){
                        if (incidencia.getNombre_departamento_reparacion().equals(user.getNombre_departamento())){
                            incidencias.add(incidencia);
                        }
                    }else if (Encriptar.desencriptar(user.getPermisos()).equals("25") || Encriptar.desencriptar(user.getPermisos()).equals("153")){
                        incidencias.add(incidencia);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return incidencias;
    }

    /**
     * Metodo que borrar el departamento
     *
     * @param departamento departamento a borrar
     */
    public void borrarDepartamento(Departamento departamento) {
        departamentos.deleteOne(objectToDocument(departamento));
    }

    /**
     * Metodo que borrar el departamento por nombre
     *
     * @param nombre departamento a borrar
     */
    public void borrarDepartamento(String nombre) {
        Departamento dp = buscarDepartamentoPorNombre(nombre);
        if (dp != null){
            borrarDepartamento(dp);
        }
    }

    /**
     * Metodo que busca el departamento por el nombre
     * @param nombre Nombre del departamento
     * @return Objeto de la clase Departamento
     */
    public Departamento buscarDepartamentoPorNombre(String nombre){
        Document query = new Document();

        query.append("nombre", nombre);

        for (Document document : departamentos.find(query)) {
            Departamento dp = documentToDepartamento(document);
            return dp;
        }
        return null;
    }

    /**
     * Metodo que busca la incidencia por el id
     * @param id Id de la incidencia
     * @return Objeto de la clase Incidencia
     */
    public Incidencia buscarIncidenciaPorId(ObjectId id){
        Document query = new Document();

        query.append("_id", id);

        for (Document document : incidencias.find(query)) {
            Incidencia inic = documentToIncidencia(document);
            return inic;
        }
        return null;
    }

    /**
     * Metodo que modifica un cliente
     *
     * @param cliente cliente a modificar
     */
    public void modificarCliente(Cliente cliente) {
        clientes.replaceOne(new Document("_id", cliente.getId()), objectToDocument(cliente));
    }

    /**
     * Metodo que modifica un tecnico
     *
     * @param tecnico tecnico a modificar
     */
    public void modificarTecnico(Tecnico tecnico) {
        tecnicos.replaceOne(new Document("_id", tecnico.getId()), objectToDocument(tecnico));
    }

    /**
     * Metodo que modifica un administrador
     *
     * @param administrador administrador a modificar
     */
    public void modificarAdmin(Administrador administrador) {
        tecnicos.replaceOne(new Document("_id", administrador.getId()), objectToDocument(administrador));
    }

    /**
     * Metodo que modifica la incidencia
     *
     * @param incidencia incidencia a modificar
     */
    public void modificarIncidencia(Incidencia incidencia) {
        incidencias.replaceOne(new Document("_id", incidencia.getId()), objectToDocument(incidencia));
    }

    /**
     * Metodo que modifica el de departamento
     *
     * @param departamento departarmento a modificar
     */
    public void modificarDepartamento(Departamento departamento) {
        departamentos.replaceOne(new Document("_id", departamento.getId()), objectToDocument(departamento));
    }

    /**
     * Metodo que modifica el nombre del departamento en todos los usuarios
     * @param antiguo Nombre antiguo del deparatento
     * @param nuevo Nuevo nombre del departamento
     */
    public void modificarNombreDepartamentoClientesTecnicos(String antiguo, String nuevo){
        for (Cliente cliente: getClientes()){
            if (cliente.getNombre_departamento().equals(antiguo)){
                cliente.setNombre_departamento(nuevo);
                modificarCliente(cliente);
            }
        }

        for (Tecnico tecnico: getTecnicos()){
            if (tecnico.getNombre_departamento().equals(antiguo)){
                tecnico.setNombre_departamento(nuevo);
                modificarTecnico(tecnico);
            }
        }
    }


    /**
     * Metodo que busca las incidencias por departamento
     * @param nombreDepartamento Nombre del departamento
     * @return ArrayList con las incidencias
     */
    public ArrayList<Incidencia> getIncidenciasDepartamento(String nombreDepartamento) {
        ArrayList<Incidencia> lista = new ArrayList<>();
        Document query = new Document();

        query.append("nombre_departamento", nombreDepartamento);

        for (Document document : incidencias.find(query)) {
            lista.add(documentToIncidencia(document));
        }
        Collections.sort(lista, new Comparator<Incidencia>() {
            public int compare(Incidencia o1, Incidencia o2) {
                if (o1.getFecha_apertura() == null || o2.getFecha_apertura() == null)
                    return 0;
                return o1.getFecha_apertura().compareTo(o2.getFecha_apertura());
            }
        });
        Collections.reverse(lista);
        return lista;
    }

    /**
     * Metodo que crea un reporte en pdf de los usuarios
     */
    public void generarPdf() {
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        for (Cliente cliente:getClientes()) {
            listaUsuarios.add(cliente);
        }
        for (Tecnico tecnico:getTecnicos()) {
            listaUsuarios.add(tecnico);
        }
        GenerarPdfUsuarios pdf = new GenerarPdfUsuarios(listaUsuarios);
        pdf.start();
    }

    /**
     * Metodo que guarda una excel con todas las incidencias
     * @throws Exception Exception
     */
    public void writeExcel() throws Exception {
        ArrayList<Incidencia> DATA = new ArrayList<Incidencia>();
        DATA = getIncidencias();

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "Hoja excel");

        String[] headers = new String[]{
                "id",
                "asunto",
                "solucion",
                "anotaciones",
                "estado",
                "descripcion",
                "fecha_apertura",
                "fecha_cierre",
                "id_tecnico",
                "id_cliente",
                "nombre_departamento",
                "nombre_departamento_reparacion"
        };

        CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);

        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        HSSFRow headerRow = sheet.createRow(0);
        for (int i = 0; i < headers.length; ++i) {
            String header = headers[i];
            HSSFCell cell = headerRow.createCell(i);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(header);
        }

        for (int i = 0; i < DATA.size(); ++i) {
            HSSFRow dataRow = sheet.createRow(i + 1);

            Incidencia inci = DATA.get(i);
            String id = String.valueOf(inci.getId());
            String asunto = inci.getAsunto();
            String solucion = inci.getSolucion();
            String anotaciones = inci.getAnotaciones();
            String estado = inci.getEstado();
            String descripcion = inci.getDescripcion();
            String fecha_apertura = (inci.getFecha_apertura() != null) ? Util.formatearFecha(inci.getFecha_apertura()) : "";
            String fecha_cierre = (inci.getFecha_cierre() != null) ? Util.formatearFecha(inci.getFecha_cierre()) : "";
            String id_tecnico = (inci.getId_tecnico() != null) ? String.valueOf(inci.getId_tecnico()) : "";
            String id_cliente = String.valueOf(inci.getId_cliente());
            String nombre_departamento = inci.getNombre_departamento();
            String nombre_departamento_reparacion = (inci.getNombre_departamento_reparacion() != null) ? inci.getNombre_departamento_reparacion() : "";

            dataRow.createCell(0).setCellValue(id);
            dataRow.createCell(1).setCellValue(asunto);
            dataRow.createCell(2).setCellValue(solucion);
            dataRow.createCell(3).setCellValue(anotaciones);
            dataRow.createCell(4).setCellValue(estado);
            dataRow.createCell(5).setCellValue(descripcion);
            dataRow.createCell(6).setCellValue(fecha_apertura);
            dataRow.createCell(7).setCellValue(fecha_cierre);
            dataRow.createCell(8).setCellValue(id_tecnico);
            dataRow.createCell(9).setCellValue(id_cliente);
            dataRow.createCell(10).setCellValue(nombre_departamento);
            dataRow.createCell(11).setCellValue(nombre_departamento_reparacion);
        }

        for (int i = 0; i < headers.length; ++i) {
            sheet.autoSizeColumn(i);
        }

        HSSFRow dataRow = sheet.createRow(1 + DATA.size());
        HSSFCell total = dataRow.createCell(1);

        JFileChooser selectorCarpeta = new JFileChooser();
        selectorCarpeta.setCurrentDirectory(new File("Desktop"));
        selectorCarpeta.setDialogTitle("Selecciona un carpeta en la que guardar el excel");
        selectorCarpeta.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        selectorCarpeta.setAcceptAllFileFilterUsed(false);
        selectorCarpeta.showOpenDialog(null);

        String ruta = selectorCarpeta.getSelectedFile() + "";

        if (!ruta.equals("null")){
            FileOutputStream file = new FileOutputStream(ruta + "/Incidencias.xls");
            workbook.write(file);
            file.close();
            Util.showWarningAlert("Incidencias descargadas correctamente");
        }


    }

    /**
     * Metodo que busca las incidencias por departamento de  reparación
     * @param nombreDepartamento Nombre del departamento de reparación
     * @return ArrayList con las incidencias
     */
    public ArrayList<Incidencia> getIncidenciasDepartamentoReparacion(String nombreDepartamento) {
        ArrayList<Incidencia> lista = new ArrayList<>();
        Document query = new Document();

        query.append("nombre_departamento_reparacion", nombreDepartamento);

        for (Document document : incidencias.find(query)) {
            lista.add(documentToIncidencia(document));
        }
        Collections.sort(lista, new Comparator<Incidencia>() {
            public int compare(Incidencia o1, Incidencia o2) {
                if (o1.getFecha_apertura() == null || o2.getFecha_apertura() == null)
                    return 0;
                return o1.getFecha_apertura().compareTo(o2.getFecha_apertura());
            }
        });
        Collections.reverse(lista);
        return lista;
    }

    /**
     * Metodo que comprueba si el login es correcto
     * @param dni dni del usuario
     * @param contrasena contraseña del usuario
     * @return En caso de que los datos sean correctos devuelve el usuario
     */
    public Usuario comprobarLogin(String dni, String contrasena) {
        ArrayList<Usuario> lista = new ArrayList<>();
        Document query = new Document();

        query.append("dni", dni);

        for (Document document : clientes.find(query)) {
            lista.add(documentToCliente(document));
        }

        if (lista.size() < 1 && lista.size() >= 0) {
            lista = new ArrayList<>();
            for (Document document : tecnicos.find(query)) {
                lista.add(documentToCliente(document));
            }
        }

        if (lista.size() == 1) {
            for (Usuario usu:lista) {
            }
            try {
                if (Encriptar.verificarhas(lista.get(0).getContrasena(), contrasena)) {
                    user = lista.get(0);
                    return lista.get(0);
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        query.clear();
        return null;
    }

    /**
     * Metodo que devuelve el usuario logueado
     * @return Usuario logueado
     */
    public Usuario getUser() {
        return user;
    }

    /**
     * Metodo que comprueba si una cadena solo contiene numeros
     * @param cadena String cadena a analizar
     * @return Boolean true o false
     */
    public boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            }catch (NumberFormatException excepcion2){
                resultado = false;
            }
        }
        return resultado;
    }

    /**
     * Metodo que rellena el ComboBox de tienda
     * @param lista lista de tiendas
     * @param combo ComboBox al que las queremos aññadir
     */
    public void setComboBoxDepartamento(ArrayList<Departamento> lista, JComboBox combo){
        combo.addItem("");
        for (Departamento depar: lista) {
            combo.addItem(depar.getNombre());
        }
        combo.setSelectedIndex(-1);
    }
}

