package com.javiergc.Proyecto.gui;

import com.javiergc.Proyecto.util.CustomeBorder;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Clase RegistroUsuariosAdmin
 */
public class RegistroUsuariosAdmin {
    private JPanel principal;
    public JPanel contenedor;
    public JFrame frame;
    public JTextField txtNombreUsu;
    public JTextField txtEmailUsu;
    public JTextField txtDniUsu;
    public JTextField txtPuestoUsu;
    public JPasswordField txtContraUsu;
    public JTextField txtApellidosUsu;
    public JTextField txtTelefonoUsu;
    public JTextField txtDireccionUsu;
    public JButton btnAtras;
    public JButton btnCrearUsu;
    public JButton btnModificarUsu;
    public JButton btnEliminarUsu;
    public JComboBox comboDepar;
    public JTable listaUsu;
    public JComboBox comboPermisos;
    public JScrollPane tablaUsuarios;
    public JTextField txtBuscar;
    public JTextField txtBuscarId;

    public DefaultTableModel dtm;

    /**
     * Constructor de la clase RegistroUsuariosAdmin
     */
    public RegistroUsuariosAdmin(){
        frame = new JFrame();
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("assets/img/eci-triangulo-logo.png"));
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setResizable(true);
        frame.setExtendedState(frame.MAXIMIZED_BOTH);

        CustomeBorder.redondearBordesTextField(txtDniUsu);
        CustomeBorder.redondearBordesTextField(txtNombreUsu);
        CustomeBorder.redondearBordesTextField(txtApellidosUsu);
        CustomeBorder.redondearBordesTextField(txtEmailUsu);
        CustomeBorder.redondearBordesTextField(txtPuestoUsu);
        CustomeBorder.redondearBordesTextField(txtTelefonoUsu);
        CustomeBorder.redondearBordesTextField(txtDireccionUsu);
        CustomeBorder.redondearBordesTextField(txtContraUsu);
        CustomeBorder.redondearBordesTextField(txtBuscar);
        CustomeBorder.redondearBordesTextField(txtBuscarId);

        tablaUsuarios.getViewport().setBackground(Color.BLACK);
        tablaUsuarios.getViewport().setForeground(Color.white);

        btnAtras.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
            }
        });

        dtm =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        listaUsu.setModel(dtm);
        txtDniUsu.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDniUsu.getText().length()>=9){
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }
}


