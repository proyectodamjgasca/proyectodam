package com.javiergc.Proyecto.gui;

import com.javiergc.Proyecto.util.CustomeBorder;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase VistaDepartamento
 */
public class VistaDepartamento {
    private JPanel principal;
    public JTextField txtNombre;
    public JTextField txtPiso;
    public JButton btnModificar;
    public JButton btnEliminar;
    public JButton btnNuevo;
    public JTable listaDepartamentos;
    private JScrollPane scroll;
    public JButton btnAtras;
    public JComboBox comboReparacion;
    public JTextField txtBuscar;
    public JFrame frame;

    public DefaultTableModel dtm;

    /**
     * Construcotr de la clase VistaDepartamento
     */
    public VistaDepartamento(){
        frame = new JFrame();
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("assets/img/eci-triangulo-logo.png"));
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setResizable(true);
        frame.setExtendedState(frame.MAXIMIZED_BOTH);

        CustomeBorder.redondearBordesTextField(txtNombre);
        CustomeBorder.redondearBordesTextField(txtPiso);
        CustomeBorder.redondearBordesTextField(txtBuscar);

        dtm =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        listaDepartamentos.setModel(dtm);

        scroll.getViewport().setBackground(Color.BLACK);
        scroll.getViewport().setForeground(Color.white);

        comboReparacion.addItem("no");
        comboReparacion.addItem("si");
    }
}
