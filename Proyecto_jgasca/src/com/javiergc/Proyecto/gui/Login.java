package com.javiergc.Proyecto.gui;

import com.javiergc.Proyecto.util.CustomeBorder;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Login
 */
public class Login {
    private Dimension dim;
    public JFrame frame;
    private JPanel principal;

    public JPanel login;
    public JPasswordField txtContraLogin;
    public JButton btnLogin;
    public JLabel lblErrorLogin;
    public JTextField txtDniLogin;

    /**
     * Constructor de la clase Vista
     */
    public Login() {
        frame = new JFrame();
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("assets/img/eci-triangulo-logo.png"));
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setVisible(true);
        frame.pack();
        frame.setExtendedState(frame.MAXIMIZED_BOTH);

        CustomeBorder.redondearBordesTextField(txtContraLogin);
        CustomeBorder.redondearBordesTextField(txtDniLogin);
    }
}
