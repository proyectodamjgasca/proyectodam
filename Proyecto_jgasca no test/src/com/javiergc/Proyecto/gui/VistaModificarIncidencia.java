package com.javiergc.Proyecto.gui;

import com.javiergc.Proyecto.util.CustomeBorder;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Clase VistaModificarIncidencia
 */
public class VistaModificarIncidencia implements WindowListener {
    public JFrame frame;
    private JPanel principal;
    public JTextField txtNombreUsu;
    public JTextField txtNombreTecnico;
    public JTextField txtDepartamentoSolicitante;
    public JComboBox comboDepartamentoEncargado;
    public JLabel lblEstado;
    public JPanel panelEstado;
    public JLabel lblFechaCierre;
    public JTextField txtAsunto;
    public JTextArea txtADescripcion;
    public JTextArea txtAAnotaciones;
    public JTextArea txtASolucion;
    public JLabel lblFechaApertura;
    public JButton btnModificarUsu;
    public JLabel lblIdInci;
    public JButton btnAsignar;
    public JButton btnSolucionar;

    /**
     * Constructor de la clase VistaModificarIncidencia
     */
    public VistaModificarIncidencia(){
        frame = new JFrame();
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setResizable(true);
        frame.setVisible(true);
        frame.pack();
        frame.setExtendedState(frame.MAXIMIZED_BOTH);

        frame.addWindowListener(this);
        CustomeBorder.redondearBordesTextField(txtAsunto);
        CustomeBorder.redondearBordesTextField(txtDepartamentoSolicitante);
        CustomeBorder.redondearBordesTextField(txtNombreTecnico);
        CustomeBorder.redondearBordesTextField(txtNombreUsu);
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Metodo que cierra el frame
     * @param e WindowEvent
     */
    @Override
    public void windowClosing(WindowEvent e) {
        frame.dispose();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
