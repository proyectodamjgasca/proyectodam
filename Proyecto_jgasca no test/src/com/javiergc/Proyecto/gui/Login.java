package com.javiergc.Proyecto.gui;

import com.javiergc.Proyecto.util.CustomeBorder;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Clase Login
 */
public class Login {
    private Dimension dim;
    public JFrame frame;
    private JPanel principal;

    public JPanel login;
    public JPasswordField txtContraLogin;
    public JButton btnLogin;
    public JLabel lblErrorLogin;
    public JTextField txtDniLogin;

    /**
     * Constructor de la clase Vista
     */
    public Login() {
        frame = new JFrame();
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setVisible(true);
        frame.pack();
        frame.setExtendedState(frame.MAXIMIZED_BOTH);

        CustomeBorder.redondearBordesTextField(txtContraLogin);
        CustomeBorder.redondearBordesTextField(txtDniLogin);
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("assets/img/eci-triangulo-logo.png"));
        /*
        try {
            //frame.setIconImage(ImageIO.read(new File("/assets/img/eci-triangulo-logo.ico")));
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        // setPreferredSize(new Dimension(800, 410));
        //frame.setLocation(-100,0);

        //dim = Toolkit.getDefaultToolkit().getScreenSize();
        //frame.pack();
        //principal.setSize(dim.width,dim.height-30);
        //frame.setSize(dim.width,dim.height);
        //frame.setSize(new Dimension(principal.getWidth()+450,principal.getHeight()));
        //frame.setLocation( (int) dim.width - frame.getWidth(), 0 );

        //frame.setLocationRelativeTo(null);


        //inicializarModelos();

        //frame.setLocationRelativeTo(null);
    }
}
