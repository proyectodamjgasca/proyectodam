package com.javiergc.Proyecto.gui;

import javax.swing.*;

/**
 * Clase VistaPanelAdmin
 */
public class VistaPanelAdmin {
    public  JFrame frame;
    public JPanel principal;
    public JButton btnRegistrarUsuarios;
    public JButton btnRegistroDepartamentos;
    public JButton btnIncidencias;
    public JButton btnCerrarSesion;
    public JButton btnDescargarIncidencias;
    public JButton btnDescargarUsuarios;

    /**
     * Constructor de la clase VistaPanelAdmin
     */
    public VistaPanelAdmin(){
        frame = new JFrame();
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setVisible(true);
        frame.pack();
        frame.setExtendedState(frame.MAXIMIZED_BOTH);
    }

}
