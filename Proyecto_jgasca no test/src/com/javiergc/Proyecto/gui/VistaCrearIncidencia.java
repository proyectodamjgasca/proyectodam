package com.javiergc.Proyecto.gui;

import com.javiergc.Proyecto.util.CustomeBorder;

import javax.swing.*;

/**
 * Clase VistaCrearIncidencia
 */
public class VistaCrearIncidencia {
    public JFrame frame;
    private JPanel principal;
    public JTextField txtUsuario;
    public JTextField txtAsunto;
    public JTextArea txtAreaDescripcion;
    public JComboBox comboBox1;
    public JButton btnAtras;
    public JButton btnModificar;
    public JButton btnCrear;

    /**
     * Constructor de la clase VistaCrearIncidencia
     */
    public VistaCrearIncidencia() {
        frame = new JFrame();
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setResizable(true);
        //frame.setExtendedState(frame.MAXIMIZED_BOTH);
        frame.setLocationRelativeTo(null);


        CustomeBorder.redondearBordesTextField(txtAsunto);
        CustomeBorder.redondearBordesTextField(txtUsuario);
    }
}
