package com.javiergc.Proyecto.gui;

import com.javiergc.Proyecto.util.CustomeBorder;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase VistaIncidencias
 */
public class VistaIncidencias {
    private JPanel principal;
    public JLabel lblIncidencias;
    public JButton tbnCrearIncidencia;
    public JTable table1;
    public JButton btnEliminar;
    public JButton btnCerrarSesion;
    public JTextField txtBuscar;
    private JScrollPane scroll;
    public JFrame frame;

    public DefaultTableModel dtm;

    /**
     * Constructor de la clase VistaIncidencias
     */
    public VistaIncidencias(){
        frame = new JFrame();
        frame.setTitle("Incidencias ElCorteInglés");
        frame.setContentPane(principal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setVisible(true);
        frame.pack();
        frame.setExtendedState(frame.MAXIMIZED_BOTH);

        dtm =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        table1.setModel(dtm);

        scroll.getViewport().setBackground(Color.BLACK);
        scroll.getViewport().setForeground(Color.white);

        CustomeBorder.redondearBordesTextField(txtBuscar);
    }
}
