package com.javiergc.Proyecto.gui.controladores;

import Encriptacion.Encriptar;
import com.javiergc.Proyecto.base.Usuario;
import com.javiergc.Proyecto.gui.Login;
import com.javiergc.Proyecto.gui.Modelo;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Clase Controlador
 */
public class ControladorLogin implements ActionListener, KeyListener, ListSelectionListener{
    private Modelo modelo;
    private Login login;

    /**
     * Constructor de la clase Controlador
     * @param modelo Modelo del programa
     * @param login Vista del programa
     */
    public ControladorLogin(Modelo modelo, Login login) {
        this.login = login;
        this.modelo = modelo;

        conectar();

        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
    }

    /**
     * Metodo que conecta con la base de datos
     */
    private void conectar(){
        if (modelo.getCliente() == null) {
            modelo.conectar();

        }
    }

    /**
     * Metodo que añade los actionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener){
        login.btnLogin.addActionListener(listener);
    }

    /**
     * Metodo que añade los SelectionListeners a las list
     * @param listener ListSelectionListener
     */
    private void addListSelectionListeners(ListSelectionListener listener){

    }

    /**
     * Metodo que añade los KeyListener a el campo de texto de buscar encargado
     * @param listener KeyListener
     */
    private void addKeyListeners(KeyListener listener){

    }

    /**
     * Metodo que se ejecuta cada vez que presionamos un boton y segun el boton presionado ejecuta distinto codigo
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "btnLogin":
                Usuario usu = modelo.comprobarLogin(login.txtDniLogin.getText(), login.txtContraLogin.getText());
                if (usu != null){
                    try {
                        if (Encriptar.desencriptar(usu.getPermisos()).equals("153")){
                            ControladorPanelAdmin con = new ControladorPanelAdmin(modelo);
                            login.frame.setVisible(false);
                        }else if (Encriptar.desencriptar(usu.getPermisos()).equals("25")){
                            ControladorListaIncidencias inici = new ControladorListaIncidencias(modelo);
                            login.frame.setVisible(false);
                        }else if (Encriptar.desencriptar(usu.getPermisos()).equals("41")){
                            ControladorListaIncidencias inici = new ControladorListaIncidencias(modelo);
                            login.frame.setVisible(false);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }else {
                    login.lblErrorLogin.setText("Error al loguearse");
                    login.lblErrorLogin.setVisible(true);
                }
                break;
        }
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
