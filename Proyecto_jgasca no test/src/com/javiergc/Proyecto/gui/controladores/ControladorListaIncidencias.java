package com.javiergc.Proyecto.gui.controladores;

import Encriptacion.Encriptar;
import com.javiergc.Proyecto.base.EditorCeldas;
import com.javiergc.Proyecto.base.Incidencia;
import com.javiergc.Proyecto.base.Usuario;
import com.javiergc.Proyecto.gui.Login;
import com.javiergc.Proyecto.gui.Modelo;
import com.javiergc.Proyecto.gui.VistaIncidencias;
import com.javiergc.Proyecto.gui.VistaModificarIncidencia;
import com.javiergc.Proyecto.util.EnviarCorreo;
import com.javiergc.Proyecto.util.Util;
import net.sf.jasperreports.engine.JRException;
import org.bson.types.ObjectId;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase ControladorListaIncidencias
 */
public class ControladorListaIncidencias implements ActionListener, KeyListener, ListSelectionListener {
    VistaIncidencias vista;
    VistaModificarIncidencia modifi;
    Modelo modelo;

    /**
     * Contructor de la clase ControladorListaIncidencias
     * @param modelo Modelo del programa
     */
    public ControladorListaIncidencias(Modelo modelo) {
        this.modelo = modelo;
        vista = new VistaIncidencias();
        modifi = new VistaModificarIncidencia();
        modifi.frame.setVisible(false);
        iniciarTabla();

        TableColumn columna = vista.table1.getColumnModel().getColumn(6);
        EditorCeldas TableCellRenderer = new EditorCeldas();
        TableCellRenderer.setColumns(6);
        columna.setCellRenderer(TableCellRenderer);

        cargarIncidenciasDepartamento();



        vista.lblIncidencias.setText("Lista incidencias " + (modelo.getUser().getNombre_departamento() != null ? modelo.getUser().getNombre_departamento() : " "));

        try {
            if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("41") || Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                vista.tbnCrearIncidencia.setVisible(false);
            }

            if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                vista.btnCerrarSesion.setIcon(new ImageIcon(getClass().getResource("/assets/img/btnAtras.PNG")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        addActionListeners(this);
        addKeyListeners(this);
        addActionListenerTable();

        try {
            switch (Encriptar.desencriptar(modelo.getUser().getPermisos())){
                case "153":
                    modifi.txtAsunto.setEditable(true);
                    modifi.txtADescripcion.setEditable(true);
                    modifi.txtAAnotaciones.setEditable(true);
                    modifi.txtASolucion.setEditable(true);
                    modifi.btnAsignar.setVisible(true);
                    break;
                case "41":
                    modifi.txtAsunto.setEditable(false);
                    modifi.txtADescripcion.setEditable(false);
                    modifi.txtAAnotaciones.setEditable(true);
                    modifi.txtASolucion.setEditable(true);
                    modifi.btnAsignar.setVisible(true);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * Metodo que añade los KeyListener a el campo de texto de buscar
     * @param listener KeyListener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscar.addKeyListener(listener);
    }

    /**
     * Metodo que añade los actionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnCerrarSesion.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.tbnCrearIncidencia.addActionListener(listener);

        modifi.btnModificarUsu.addActionListener(listener);
        modifi.btnAsignar.addActionListener(listener);
        modifi.btnSolucionar.addActionListener(listener);
    }

    /**
     * Metodo que ejecuta acciones cuando se hace soble click en la tabla
     */
    private void addActionListenerTable(){
        vista.table1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount()> 1){
                    modifi.frame.setVisible(true);
                    int fila = vista.table1.getSelectedRow();
                    cargarDatosVistaModificarIncidencia(modifi, (ObjectId) vista.dtm.getValueAt(fila,0));
                    try {
                        if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("41") || Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                            if (!modifi.txtNombreTecnico.equals("")){
                                modifi.btnSolucionar.setVisible(true);
                            }
                        }
                        modifi.frame.setVisible(true);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * Metodo que carga los datos de la inciencia en la vista VistaModificarIncidencia
     * @param modifi Vista VistaModificarIncidencia
     */
    private void cargarDatosVistaModificarIncidencia(VistaModificarIncidencia modifi, ObjectId idIncidencia) {
        modifi.comboDepartamentoEncargado.removeAllItems();
        modelo.setComboBoxDepartamento(modelo.getDepartamentosReparacion(),modifi.comboDepartamentoEncargado);

        Incidencia inici = modelo.buscarIncidenciaPorId(idIncidencia);

        modifi.txtDepartamentoSolicitante.setText(inici.getNombre_departamento());
        modifi.comboDepartamentoEncargado.setSelectedItem(inici.getNombre_departamento_reparacion());
        modifi.lblEstado.setText(inici.getEstado());
        switch (inici.getEstado()){
            case "Pendiente":
                modifi.panelEstado.setBackground(Color.gray);
                break;
            case "Procesando":
                modifi.panelEstado.setBackground(Color.yellow);
                break;

            case "Solucionado":
                modifi.panelEstado.setBackground(Color.green);
                break;
        }
        Usuario cliente = (Usuario) modelo.buscarUsuarioPorId(inici.getId_cliente()).get(0);
        modifi.txtNombreUsu.setText(cliente.getNombre() + " " + cliente.getApellidos());
        Usuario tecnico = null;
        try{
            tecnico = (Usuario) modelo.buscarUsuarioPorId(inici.getId_tecnico()).get(0);
        }catch (NullPointerException e){

        }
        if (tecnico != null){
            modifi.txtNombreTecnico.setText(tecnico.getNombre() + " " + tecnico.getApellidos());
            modifi.btnAsignar.setIcon(new ImageIcon(getClass().getResource("/assets/img/btnDesasignar.PNG")));
        }else {
            modifi.txtNombreTecnico.setText("");
            modifi.btnAsignar.setIcon(new ImageIcon(getClass().getResource("/assets/img/btnAsignar.PNG")));
        }
        modifi.lblFechaApertura.setText(Util.formatearFecha(inici.getFecha_apertura()));
        if (inici.getFecha_cierre() != null){
            modifi.lblFechaCierre.setText(Util.formatearFecha(inici.getFecha_cierre()));
        }
        modifi.txtAsunto.setText(inici.getAsunto());
        modifi.txtADescripcion.setText(inici.getDescripcion());
        modifi.txtAAnotaciones.setText(inici.getAnotaciones());
        modifi.txtASolucion.setText(inici.getSolucion());
        modifi.lblIdInci.setText(inici.getId().toString());
    }

    /**
     * Metodo que ejecuta las acciones correspondientes al pulsar un boton
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "crearIncidencia":
                ControladorCrearIncidencia crea = new ControladorCrearIncidencia(modelo, this);
                break;
            case "eliminar":
                int filaBorrar = vista.table1.getSelectedRow();
                ObjectId idBorrar = (ObjectId) vista.dtm.getValueAt(filaBorrar,0);
                modelo.borrarIncidencia(modelo.buscarIncidenciaPorId(idBorrar));
                break;

            case "cerrarSesion":
                try {
                    if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                       ControladorPanelAdmin admin = new ControladorPanelAdmin(modelo);
                       modifi.frame.dispose();
                       vista.frame.dispose();
                    }else {
                        modelo.desconectar();
                        modelo = null;
                        vista.frame.dispose();
                        modifi.frame.dispose();
                        ControladorLogin login = new ControladorLogin(new Modelo(), new Login());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case "ModificarUsu":
                Incidencia inci = modelo.buscarIncidenciaPorId(new ObjectId(modifi.lblIdInci.getText()));
                try {
                    if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("25") || Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                        if (!modifi.comboDepartamentoEncargado.getSelectedItem().toString().equals("") && !modifi.txtAsunto.getText().equals("") && !modifi.txtADescripcion.getText().equals("")){
                            inci.setNombre_departamento_reparacion(modifi.comboDepartamentoEncargado.getSelectedItem().toString());
                            inci.setAsunto(modifi.txtAsunto.getText());
                            inci.setDescripcion(modifi.txtADescripcion.getText());
                        }else {
                            Util.showWarningAlert("Falta algun campo por rellenar");
                            break;
                        }
                    }
                    if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("41") || Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                        if (!modifi.comboDepartamentoEncargado.getSelectedItem().toString().equals("")){
                            inci.setNombre_departamento_reparacion(modifi.comboDepartamentoEncargado.getSelectedItem().toString());
                            inci.setAnotaciones(modifi.txtAAnotaciones.getText());
                            inci.setSolucion(modifi.txtASolucion.getText());
                        }else {
                            Util.showWarningAlert("Falta algun campo por rellenar");
                            break;
                        }
                    }

                    modelo.modificarIncidencia(inci);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case "asignarTecnico":
                Incidencia incidencia = modelo.buscarIncidenciaPorId(new ObjectId(modifi.lblIdInci.getText()));
                if (!modifi.txtNombreTecnico.getText().equals("")){
                    incidencia.setId_tecnico(null);
                    incidencia.setEstado("Pendiente");
                    modifi.btnSolucionar.setVisible(false);
                    modelo.modificarIncidencia(incidencia);
                }else {
                    incidencia.setId_tecnico(modelo.getUser().getId());
                    incidencia.setEstado("Procesando");
                    modifi.btnSolucionar.setVisible(true);
                    modelo.modificarIncidencia(incidencia);
                }
                break;
            case "solucionar":
                if (!modifi.lblEstado.getText().equals("Solucionado")){
                    if (!modifi.txtNombreTecnico.getText().equals("")){
                        if (!modifi.txtASolucion.getText().equals("")){
                            Incidencia incidenciaSolu = modelo.buscarIncidenciaPorId(new ObjectId(modifi.lblIdInci.getText()));
                            incidenciaSolu.setEstado("Solucionado");
                            incidenciaSolu.setFecha_cierre(LocalDate.now());
                            incidenciaSolu.setSolucion(modifi.txtASolucion.getText());
                            modelo.modificarIncidencia(incidenciaSolu);
                            Usuario usu = (Usuario) modelo.buscarUsuarioPorId(incidenciaSolu.getId_cliente()).get(0);
                            Usuario usuTecnico = (Usuario) modelo.buscarUsuarioPorId(incidenciaSolu.getId_tecnico()).get(0);
                            String descripcion = "Incidencias con asunto '" + incidenciaSolu.getAsunto() + "' ha sido solucionada por el tecnico/@ " +  usuTecnico.getNombre() + " " +usuTecnico.getApellidos() + "\nYa puede revisar la solución de la incidencia en el programa \nGracias por el aporte";
                            String asunto = "*** Incidencia solucionada ***";
                            EnviarCorreo correo = new EnviarCorreo(asunto,descripcion,usu.getEmail());
                            correo.start();
                        }else {
                            Util.showWarningAlert("No has aportado ninguna solución");
                        }
                    }else {
                        Util.showWarningAlert("No hay ningun tecnico asignado a la incidencia");
                    }
                }else {
                    Util.showWarningAlert("La incidencia ya esta solucionada");
                }
                break;
        }
        if (!e.getActionCommand().equals("cerrarSesion")){
            if (modifi.frame.isShowing()){
                cargarDatosVistaModificarIncidencia(modifi,new ObjectId(modifi.lblIdInci.getText()));
            }
            cargarIncidenciasDepartamento();
        }
    }

    /**
     * Metodo que carga las filas de incidencias segun el deparatmento del usuario registrado
     */
    public void cargarIncidenciasDepartamento(){
        try {
            if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("41")){
                cargarFilas(modelo.getIncidenciasDepartamentoReparacion(modelo.getUser().getNombre_departamento()));
            }else if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                cargarFilas(modelo.getIncidencias());
            }else {
                cargarFilas(modelo.getIncidenciasDepartamento(modelo.getUser().getNombre_departamento()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que establece las cabeceras de la tabla
     */
    private void iniciarTabla(){
        String[] headers = {"Id", "Usuario","Asunto", "Departamento", "Fecha apertura", "Fecha cierre","Estado"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Metodo que carga los datos recibios en un JTable
     * @param lista Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    protected void cargarFilas(ArrayList<Incidencia> lista) {
        Object[] fila = new Object[7];
        vista.dtm.setRowCount(0);

        for (Incidencia incidencia: lista) {
            fila[0]=incidencia.getId();
            Usuario usu = (Usuario) modelo.buscarUsuarioPorId(incidencia.getId_cliente()).get(0);

            fila[1]=usu.getNombre() + " " + usu.getApellidos() ;
            fila[2]=incidencia.getAsunto();
            try {
                if (Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("41") || Encriptar.desencriptar(modelo.getUser().getPermisos()).equals("153")){
                    fila[3]=incidencia.getNombre_departamento();
                }else {
                    fila[3]=incidencia.getNombre_departamento_reparacion();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            fila[4]=Util.formatearFecha(incidencia.getFecha_apertura());
            fila[5]=(incidencia.getFecha_cierre() != null) ? Util.formatearFecha(incidencia.getFecha_cierre()) : "";
            fila[6]=incidencia.getEstado();

            vista.dtm.addRow(fila);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Metodo que se ejecuta cuando se presiona una tecla en el txtBuscar
     * @param e KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscar) {
            if (!vista.txtBuscar.getText().equals("")){
                String busqueda = vista.txtBuscar.getText();
                cargarFilas(modelo.buscarIncidenciasUsuario(busqueda));
            }else {
                cargarIncidenciasDepartamento();
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
