package com.javiergc.Proyecto.gui.controladores;

import Encriptacion.Encriptar;
import com.javiergc.Proyecto.base.*;
import com.javiergc.Proyecto.gui.Modelo;
import com.javiergc.Proyecto.gui.RegistroUsuariosAdmin;
import com.javiergc.Proyecto.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase ControladorRegistroUsu
 */
public class ControladorRegistroUsu implements ActionListener, KeyListener, ListSelectionListener {
    private Modelo modelo;
    private RegistroUsuariosAdmin vista;
    private Usuario usuSeleccinado;

    /**
     * Constructor de la clase ControladorRegistroUsu
     * @param modelo Modelo del programa
     */
    public ControladorRegistroUsu(Modelo modelo) {
        this.modelo = modelo;
        this.vista = new RegistroUsuariosAdmin();

        addActionListeners(this);
        addActionListenerTable();
        addKeyListeners(this);
        iniciarTabla();
        rellenarPermisos();
        listenerCombo();
        cargarFilas(modelo.getClientes(),modelo.getTecnicos());
    }

    /**
     * Metodo que añade los KeyListener a el campo de texto de buscar
     * @param listener KeyListener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscar.addKeyListener(listener);
        vista.txtBuscarId.addKeyListener(listener);
    }

    /**
     * Metodo que añade los actionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnCrearUsu.addActionListener(listener);
        vista.btnEliminarUsu.addActionListener(listener);
        vista.btnModificarUsu.addActionListener(listener);
        vista.btnAtras.addActionListener(listener);
    }

    /**
     * Metodo que esta a la escucha de cuando se selecciona una fila del JTable
     */
    private void addActionListenerTable(){
        vista.listaUsu.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vista.listaUsu.getSelectedRow();
                int col = vista.listaUsu.getSelectedColumn();
                Usuario usu = (Usuario) modelo.buscarUsuarioPorDni((String) vista.listaUsu.getValueAt(row,3)).get(0);
                usuSeleccinado = usu;
                vista.txtNombreUsu.setText(usu.getNombre());
                vista.txtApellidosUsu.setText(usu.getApellidos());
                vista.txtEmailUsu.setText(usu.getEmail());
                vista.txtTelefonoUsu.setText(usu.getTelefono());
                vista.txtDniUsu.setText(usu.getDni());
                vista.txtDireccionUsu.setText(usu.getDireccion());
                vista.txtPuestoUsu.setText(usu.getPuesto());

                try {
                    switch (Encriptar.desencriptar(usu.getPermisos())){
                        case "25":
                            vista.comboPermisos.setSelectedIndex(2);
                            break;

                        case "41":
                            vista.comboPermisos.setSelectedIndex(3);
                            break;

                        case "153":
                            vista.comboPermisos.setSelectedIndex(1);
                            break;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                Departamento dep = modelo.buscarDepartamentoPorNombre(usu.getNombre_departamento());
                vista.comboDepar.setSelectedItem((String) vista.listaUsu.getValueAt(row,5));
            }
        });
    }

    /**
     * Metodo que se ejecuta cada vez que presionamos un boton y segun el boton presionado ejecuta distinto codigo
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "NuevoUsu":
                if (vista.comboPermisos.getSelectedItem() != null && !vista.comboPermisos.getSelectedItem().equals("")){
                    switch (vista.comboPermisos.getSelectedItem().toString()){
                        case "Administrador":
                            Administrador admin= new Administrador();
                           if (modificarUsuarioFromCampos(admin)){
                               admin.setPermisosCifrado("153");
                               modelo.guardarObjeto(admin);

                               limpiarCampos();
                               cargarFilas(modelo.getClientes(),modelo.getTecnicos());
                               vista.comboDepar.removeAllItems();
                           }
                            break;

                        case "Cliente":
                            Cliente cliente = new Cliente();
                            if (modificarUsuarioFromCampos(cliente)){
                                cliente.setPermisosCifrado("25");
                                modelo.guardarObjeto(cliente);

                                limpiarCampos();
                                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
                                vista.comboDepar.removeAllItems();
                            }
                            break;

                        case "Tecnico":
                            Tecnico tecnico = new Tecnico();
                            if (modificarUsuarioFromCampos(tecnico)){
                                tecnico.setPermisosCifrado("41");
                                modelo.guardarObjeto(tecnico);

                                limpiarCampos();
                                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
                                vista.comboDepar.removeAllItems();
                            }
                            break;

                    }
                }else {
                    Util.showWarningAlert("Primero tienes que elegir el permiso del usuario");
                }

                break;
            case "EliminarUsu":
                int filaBorrar = vista.listaUsu.getSelectedRow();
                String dniBorrar = (String) vista.dtm.getValueAt(filaBorrar,3);
                modelo.borrarUsuarioPorDni(dniBorrar);
                limpiarCampos();
                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
                vista.comboDepar.removeAllItems();
                break;
            case "ModificarUsu":
                String permiso = "";
                try {
                    permiso = Encriptar.desencriptar(usuSeleccinado.getPermisos());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if (!vista.txtDniUsu.getText().equals("")){
                    switch (permiso){
                        case "153":
                            Administrador admin = new Administrador();
                            if (!vista.txtDniUsu.getText().equals("")){
                                admin = (Administrador) modelo.buscarUsuarioPorDni(vista.txtDniUsu.getText()).get(0);
                            }
                            if (modificarUsuarioFromCampos2(admin)){
                                Administrador administrador = (Administrador) modelo.buscarUsuarioPorDni(admin.getDni()).get(0);
                                if (admin.getContrasena().equals("")){
                                    admin.setContrasena(administrador.getContrasena());
                                }
                                //falta || Hay que arreglar lo de los permisos que hay que establecerlos en cada modificacion
                                admin.setId(administrador.getId());
                                modelo.modificarAdmin(admin);
                                limpiarCampos();
                                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
                                vista.comboDepar.removeAllItems();
                                usuSeleccinado = null;
                            }
                            break;

                        case "25":
                            Cliente cliente = new Cliente();
                            if (!vista.txtDniUsu.getText().equals("")){
                                cliente = (Cliente) modelo.buscarUsuarioPorDni(vista.txtDniUsu.getText()).get(0);
                            }
                            if (modificarUsuarioFromCampos2(cliente)){
                                Cliente cliente2 = (Cliente) modelo.buscarUsuarioPorDni(cliente.getDni()).get(0);
                                if (cliente.getContrasena().equals("")){
                                    cliente.setContrasena(cliente2.getContrasena());
                                }
                                //falta || Hay que arreglar lo de los permisos que hay que establecerlos en cada modificacion
                                cliente.setId(cliente2.getId());
                                modelo.modificarCliente(cliente);
                                limpiarCampos();
                                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
                                vista.comboDepar.removeAllItems();
                                usuSeleccinado = null;
                            }
                            break;

                        case "41":
                            Tecnico tecnico = new Tecnico();
                            if (!vista.txtDniUsu.getText().equals("")){
                                tecnico = (Tecnico) modelo.buscarUsuarioPorDni(vista.txtDniUsu.getText()).get(0);
                            }
                            if (modificarUsuarioFromCampos2(tecnico)){
                                Tecnico tecnico1 = (Tecnico) modelo.buscarUsuarioPorDni(tecnico.getDni()).get(0);
                                if (tecnico.getContrasena().equals("")){
                                    tecnico.setContrasena(tecnico1.getContrasena());
                                }
                                //falta || Hay que arreglar lo de los permisos que hay que establecerlos en cada modificacion
                                tecnico.setId(tecnico1.getId());
                                modelo.modificarTecnico(tecnico);
                                limpiarCampos();
                                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
                                vista.comboDepar.removeAllItems();
                                usuSeleccinado = null;
                            }
                            break;
                    }
                }
                break;

            case "btnAtras":
                ControladorPanelAdmin admin = new ControladorPanelAdmin(modelo);
                vista.frame.dispose();
                break;
        }
    }

    /**
     * Meotodo que modifica un usuario con los datos del programa
     * @param usu Usuario a modificar
     * @return true o false si se ha completado o no
     */
    private boolean modificarUsuarioFromCampos(Usuario usu) {
        Encriptar enc = new Encriptar();
        if (!vista.txtNombreUsu.getText().equals("") &&  !vista.txtApellidosUsu.getText().equals("") && !vista.txtEmailUsu.getText().equals("") && !vista.txtTelefonoUsu.getText().equals("") && !vista.txtDniUsu.getText().equals("") &&
                !vista.txtDireccionUsu.getText().equals("") && !vista.txtPuestoUsu.getText().equals("") && !vista.txtPuestoUsu.getText().equals("") && vista.comboDepar.getSelectedItem() != null && !vista.txtContraUsu.getText().equals("")
                && vista.comboPermisos.getSelectedItem() != null){
            if (!comprobarDni(vista.txtDniUsu.getText())){
                usu.setNombre(vista.txtNombreUsu.getText());
                usu.setApellidos(vista.txtApellidosUsu.getText());
                usu.setEmail(vista.txtEmailUsu.getText());
                usu.setTelefono(vista.txtTelefonoUsu.getText());
                usu.setDni(vista.txtDniUsu.getText());
                usu.setDireccion(vista.txtDireccionUsu.getText());
                usu.setPuesto(vista.txtPuestoUsu.getText());
                //Departamento dep = (Departamento) vista.comboDepar.getSelectedItem();
                usu.setNombre_departamento(vista.comboDepar.getSelectedItem().toString());
                usu.setContrasenaHaseada(vista.txtContraUsu.getText());
                usu.setFechaAlta(LocalDate.now());
            }else {
                Util.showErrorAlert("El dni introducido ya existe");
                return false;
            }

            return true;
        }else {
            Util.showWarningAlert("Faltan campos por rellenar");
        }
        return false;
    }

    /**
     * Meotodo que modifica un usuario con los datos del programa pero sin tener en cuenta si la contraseña esta vacio (Opcion para la modificación)
     * @param usu Usuario a modificar
     * @return true o false si se ha completado o no
     */
    private boolean modificarUsuarioFromCampos2(Usuario usu) {
        Encriptar enc = new Encriptar();
        if (!vista.txtNombreUsu.getText().equals("") &&  !vista.txtApellidosUsu.getText().equals("") && !vista.txtEmailUsu.getText().equals("") && !vista.txtTelefonoUsu.getText().equals("") && !vista.txtDniUsu.getText().equals("") &&
                !vista.txtDireccionUsu.getText().equals("") && !vista.txtPuestoUsu.getText().equals("") && !vista.txtPuestoUsu.getText().equals("") && vista.comboDepar.getSelectedItem() != null
                && vista.comboPermisos.getSelectedItem() != null){
            usu.setNombre(vista.txtNombreUsu.getText());
            usu.setApellidos(vista.txtApellidosUsu.getText());
            usu.setEmail(vista.txtEmailUsu.getText());
            usu.setTelefono(vista.txtTelefonoUsu.getText());
            usu.setDni(vista.txtDniUsu.getText());
            usu.setDireccion(vista.txtDireccionUsu.getText());
            usu.setPuesto(vista.txtPuestoUsu.getText());
            //Departamento dep = (Departamento) vista.comboDepar.getSelectedItem();
            usu.setNombre_departamento(vista.comboDepar.getSelectedItem().toString());
            if (!vista.txtContraUsu.getText().equals("")){
                usu.setContrasenaHaseada(vista.txtContraUsu.getText());
            }else {
                usu.setContrasena("");
            }

            usu.setFechaAlta(LocalDate.now());
            return true;
        }else {
            Util.showWarningAlert("Hay campos vacios");
        }
        return false;
    }

    /**
     * Metodo que establece las cabeceras de la tabla
     */
    private void iniciarTabla(){
        String[] headers = {"Id", "Nombre", "Apellidos", "Dni", "Puesto","Departamento","Telefono", "Permisos", "Fecha alta"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Metodo que carga los datos recibios en un JTable
     * @param lista Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    private void cargarFilas(ArrayList<Cliente> lista, ArrayList<Tecnico> lista2) {
        Object[] fila = new Object[9];
        vista.dtm.setRowCount(0);

        for (Usuario usu: lista) {
            fila[0]=usu.getId();
            fila[1]=usu.getNombre();
            fila[2]=usu.getApellidos();
            fila[3]=usu.getDni();
            fila[4]=usu.getPuesto();
            fila[5]=usu.getNombre_departamento();
            fila[6]=usu.getTelefono();
            try {
                fila[7]= Encriptar.desencriptar(usu.getPermisos());
            } catch (Exception e) {
                e.printStackTrace();
            }

            fila[8]=Util.formatearFecha(usu.getFechaAlta());


            vista.dtm.addRow(fila);
        }

        for (Usuario usu: lista2) {
            fila[0]=usu.getId();
            fila[1]=usu.getNombre();
            fila[2]=usu.getApellidos();
            fila[3]=usu.getDni();
            fila[4]=usu.getPuesto();
            fila[5]=usu.getNombre_departamento();
            fila[6]=usu.getTelefono();
            try {
                fila[7]= Encriptar.desencriptar(usu.getPermisos());
            } catch (Exception e) {
                e.printStackTrace();
            }
            fila[8]=Util.formatearFecha(usu.getFechaAlta());


            vista.dtm.addRow(fila);
        }
    }

    /**
     * Metodo que carga los datos recibios en un JTable
     * @param lista Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    private void cargarFilas(ArrayList<Usuario> lista) {
        Object[] fila = new Object[9];
        vista.dtm.setRowCount(0);

        if (lista != null){
            for (Usuario usu: lista) {
                fila[0]=usu.getId();
                fila[1]=usu.getNombre();
                fila[2]=usu.getApellidos();
                fila[3]=usu.getDni();
                fila[4]=usu.getPuesto();
                fila[5]=usu.getNombre_departamento();
                fila[6]=usu.getTelefono();
                try {
                    fila[7]= Encriptar.desencriptar(usu.getPermisos());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fila[8]=Util.formatearFecha(usu.getFechaAlta());


                vista.dtm.addRow(fila);
            }
        }
    }

    /**
     * Metodo que rellena el combobox con los departamentos segun el permiso elegido
     */
    public void rellenarCombo(String permiso){
        vista.comboDepar.removeAllItems();
        switch (vista.comboPermisos.getSelectedItem().toString()){
            case "Administrador":
                vista.comboDepar.addItem("Administración");
                //modelo.setComboBoxDepartamento(modelo.getDepartamentosReparacion(),vista.comboDepar);
                break;
            case "Tecnico":
                ArrayList<Departamento> tecnicos = modelo.getDepartamentosReparacion();
                ArrayList<Departamento> tecnicos2 = modelo.getDepartamentosReparacion();

                for (int i=0; i < tecnicos.size() ;i++){
                    if (tecnicos.get(i).getNombre().equals("Administración")){
                        tecnicos2.remove(i);
                    }
                }
                modelo.setComboBoxDepartamento(tecnicos2,vista.comboDepar);
                break;
            case "Cliente":
                modelo.setComboBoxDepartamento(modelo.getDepartamentosNoReparacion(),vista.comboDepar);
                break;
        }
    }

    /**
     * Metodo que rellena el combobox con los departamentos
     */
    public void rellenarPermisos(){
        vista.comboPermisos.removeAllItems();
        vista.comboPermisos.addItem("");
        vista.comboPermisos.addItem("Administrador");
        vista.comboPermisos.addItem("Cliente");
        vista.comboPermisos.addItem("Tecnico");
    }

    private void limpiarCampos(){
        vista.txtNombreUsu.setText("");
        vista.txtApellidosUsu.setText("");
        vista.txtDireccionUsu.setText("");
        vista.txtPuestoUsu.setText("");
        vista.txtEmailUsu.setText("");
        vista.txtContraUsu.setText("");
        vista.comboPermisos.setSelectedIndex(0);
        vista.txtDniUsu.setText("");
        vista.txtTelefonoUsu.setText("");
    }


    /**
     * Metodo que comprueba que el dni del nuevo usuario que no coincida con uno existente
     * @param dni Dni del nuevo Usuario a comprobar
     * @return true o false
     */
    private boolean comprobarDni(String dni) {
        for (Cliente dp:modelo.getClientes()) {
            if (dp.getDni().toLowerCase().equals(dni.toLowerCase())){
                return true;
            }
        }
        for (Tecnico dp:modelo.getTecnicos()) {
            if (dp.getDni().toLowerCase().equals(dni.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Metodo que captura cuando se levanta una letra pulsada
     * @param e KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscar) {
            if (vista.txtBuscar.getText().equals("")){
                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
            }else {
                cargarFilas(modelo.getUsuarios(vista.txtBuscar.getText(),false));
            }
        }

        if(e.getSource() == vista.txtBuscarId){
            if (!vista.txtBuscarId.getText().equals("")){
                cargarFilas(modelo.buscarUsuarioPorId2(vista.txtBuscarId.getText(), true));
            }else {
                cargarFilas(modelo.getClientes(),modelo.getTecnicos());
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    /**
     * Metodo que detecta cambios en el comboPermisos y carga con datos el comboBox comboDepar segun la elección
     */
    public void listenerCombo(){
        vista.comboPermisos.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                rellenarCombo(vista.comboPermisos.getSelectedItem().toString());
            }
        });
    }
}
