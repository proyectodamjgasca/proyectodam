package com.javiergc.Proyecto.base;

import Encriptacion.Encriptar;
import org.bson.types.ObjectId;

import java.time.LocalDate;

/**
 * Clase Usuarios
 */
public class Usuario {
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private String email;
    private String telefono;
    private String dni;
    private String direccion;
    private String puesto;
    private String permisos;
    private String contrasena;
    private String nombre_departamento;
    private LocalDate fechaAlta;

    /**
     * Constructor de la clase Usuario
     * @param nombre Nombre del usuario
     * @param apellidos Apellidos del usuario
     * @param email Email del usuario
     * @param telefono Telefono del usuario
     * @param dni Dni del usuario
     * @param direccion Dirección del usuario
     * @param puesto Puesto del usuario
     * @param nombre_departamento
     * @param fechaAlta fecha de alta del usuario
     */
    public Usuario(String nombre, String apellidos, String email, String telefono, String dni, String direccion, String puesto, String contrasena,String nombre_departamento, LocalDate fechaAlta) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.telefono = telefono;
        this.dni = dni;
        this.direccion = direccion;
        this.puesto = puesto;
        this.nombre_departamento = nombre_departamento;
        this.fechaAlta = fechaAlta;
        this.contrasena = contrasena;
    }

    /**
     * Constructor vacio de la clase Usuario
     */
    public Usuario(){}

    /**
     * Metodo que compara el dni de un usuario introducido
     * @param usuario usuario a comparar su dni
     * @return True si son iguales o false si no lo son
     */
    public boolean compararDni(Usuario usuario) {
        return dni.equals(usuario.getDni());
    }

    /**
     * Metodo que devuelve el nombre del usuario
     * @return Nombre del usuario
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre del usuario
     * @param nombre Nombre del usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve los apellidos del usuario
     * @return Apellidos del usuario
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Metodo que establece los apellidos del usuario
     * @param apellidos Apellidos del usuario
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Metodo que devuelve el email de usuario
     * @return Email de usuario
     */
    public String getEmail() {
        return email;
    }

    /**
     * Metodo que establece el email del usuario
     * @param email Email del usuario
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Metodo que devuelve el telefono del usuario
     * @return Telefono del usuario
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo que estable el telefono del usuario
     * @param telefono Telefono del usuario
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que devuelve el dni del usuario
     * @return Dni del usuario
     */
    public String getDni() {
        return dni;
    }

    /**
     * Metodo que establece el dni del usuario
     * @param dni Dni del usuario
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Metodo que devuelve el id del usuario
     * @return Id del usuario
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Metodo que establece el id del usuario
     * @param id Id del usuario
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve la fecha de alta del usuario
     * @return Fecha de alta del usuario
     */
    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    /**
     * Metodo establece la fecha de alta de un usuario
     * @param fechaAlta
     */
    public void setFechaAlta(LocalDate fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * Metodo que devuelve la dirección del usuario
     * @return Dirección del usuario
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo que establece la dirección del usuario
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo que devuelve el puesto el usuario
     * @return Puesto del usuario
     */
    public String getPuesto() {
        return puesto;
    }

    /**
     * Metodo que establece el puesto del usuario
     * @param puesto
     */
    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    /**
     * Metodo que devuelve los permisos de del usuario
     * @return Permiso del usuario
     */
    public String getPermisos() {
        return permisos;
    }

    /**
     * Metodo que establece los permisos del usuario cifrados
     * @param permisos Permiso del usuario
     */
    public void setPermisosCifrado(String permisos) {
        try {
            this.permisos = Encriptar.encriptar(permisos);
        } catch (Exception e) {
            System.err.println("** Error las encriptar permisos **");
            e.printStackTrace();
        }
    }

    /**
     * Metodo que establece los permisos del usuario
     * @param permisos Permiso del usuario
     */
    public void setPermisos(String permisos) {
        this.permisos = permisos;
    }

    /**
     * Metodo que devuelve la contraseña el usuario
     * @return Contraseña del usuario
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * Metodo que establece la contraseña del usuarios haseados
     * @param contrasena contraseña del usuario
     */
    public void setContrasenaHaseada(String contrasena) {
        try {
            this.contrasena = Encriptar.hasear(contrasena);
        } catch (Exception e) {
            System.err.println("** Error las hasear la contraseña **");
            e.printStackTrace();
        }
    }

    /**
     * Metodo que establece la contraseña del usuarios
     * @param contrasena contraseña del usuario
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * Metodo que devuelve el nombre del departamento al que pertenece el usuario
     * @return Nombre del departamento al que pertenece el usuario
     */
    public String getNombre_departamento() {
        return nombre_departamento;
    }

    /**
     * Metodo que establece el nombre del departamento al que pertenece el usuario
     * @param nombre_departamento Nombre del departamento al que va a pertenecer el usuario
     */
    public void setNombre_departamento(String nombre_departamento) {
        this.nombre_departamento = nombre_departamento;
    }

    /**
     * Metodo que devuelve el Usuario pasado a String
     *
     * @return String con los atributos
     */
    @Override
    public String toString() {
        return "Usuario{" +
                "id-> " + id +
                ", nombre-> " + nombre +
                ", apellidos-> " + apellidos +
                ", email='" + email +
                ", telefono-> " + telefono +
                ", dni-> " + dni +
                ", direccion-> " + direccion +
                ", puesto-> " + puesto +
                ", permisos-> " + permisos +
                ", nombre_departamento-> " + nombre_departamento +
                ", fechaAlta=" + fechaAlta +
                '}';
    }


}
