package com.javiergc.Proyecto.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

/**
 * Clase Incidencia
 */
public class Incidencia {
    private ObjectId id;
    private String asunto;
    private String solucion;
    private String anotaciones;
    private String estado;
    private String descripcion;
    private LocalDate fecha_apertura;
    private LocalDate fecha_cierre;
    private ObjectId id_tecnico;
    private ObjectId id_cliente;
    private String nombre_departamento;
    private String nombre_departamento_reparacion;

    /**
     * Constructor de la clase Incidencia
     * @param asunto Asunto de la inicdencia
     * @param descripción Descripción
     * @param fecha_apertura Fecha en la que se abre la incidencia
     * @param id_cliente id del cliente que abre la inicdencia
     * @param nombre_departamento_reparacion Nombre del departamento que debe solucionar la incidencia
     */
    public Incidencia(String asunto, String descripción, LocalDate fecha_apertura, ObjectId id_cliente, String nombre_departamento,String nombre_departamento_reparacion) {
        this.asunto = asunto;
        this.descripcion = descripción;
        this.fecha_apertura = fecha_apertura;
        this.id_cliente = id_cliente;
        this.nombre_departamento_reparacion = nombre_departamento_reparacion;
    }

    /**
     * Constructor vacio de la clase incidencia
     */
    public Incidencia(){}

    /**
     * Metodo que devuelve el id de la incidencia
     * @return Id de la incidencia
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Metodo que establece el id de la incidencia
     * @param id ID de la incidencia
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el asunto de la incidencia
     * @return Asunto de la incidencia
     */
    public String getAsunto() {
        return asunto;
    }

    /**
     * Metodo que establece el asunto de la incidencia
     * @param asunto Asunto de la incidencia
     */
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    /**
     * Metodo que devuelve la solución de la incidencia
     * @return Solución de la incidencia
     */
    public String getSolucion() {
        return solucion;
    }

    /**
     * Metodo que establece la solución de la incidencia
     * @param solucion Solición de la incidencia
     */
    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }

    /**
     * Metodo que devuelve las anotaciones de la incidencia
     * @return Anotaciones de la incidencia
     */
    public String getAnotaciones() {
        return anotaciones;
    }

    /**
     * Metodo que establece las anotaciones de la incidencia
     * @param anotaciones Anotaciones de la incidencia
     */
    public void setAnotaciones(String anotaciones) {
        this.anotaciones = anotaciones;
    }

    /**
     * Metodo que devuelve el estado de la incidencia
     * @return Estado de la incidencia
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo que establece el estado de la incidencia
     * @param estado Estado de la incidencia
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo que devuelve la descripcion de la incidencia
     * @return Descripción de la incidencia
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo que establece la descripción de la incidencia
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Metodo que devuelve la fecha_apertura de la incidencia
     * @return Fecha de apertura de la incidencia
     */
    public LocalDate getFecha_apertura() {
        return fecha_apertura;
    }

    /**
     * Metodo que establece la fecha_apertura de la incidencia
     * @param fecha_apertura Fecha de apertura de la incidencia
     */
    public void setFecha_apertura(LocalDate fecha_apertura) {
        this.fecha_apertura = fecha_apertura;
    }

    /**
     * Metodo que devuelve la fecha de cierre de la incidencia
     * @return Fecha de cierre de la incidencia
     */
    public LocalDate getFecha_cierre() {
        return fecha_cierre;
    }

    /**
     * Metodo que establece la fecha de cierre de la incidencia
     * @param fecha_cierre Fecha de cierre de la incidencia
     */
    public void setFecha_cierre(LocalDate fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }

    /**
     * Metodo que devuelve el id de la incidencia
     * @return Id de la incidencia
     */
    public ObjectId getId_tecnico() {
        return id_tecnico;
    }

    /**
     * Metodo que establece el id de la incidencia
     * @param id_tecnico Id de la inidencia
     */
    public void setId_tecnico(ObjectId id_tecnico) {
        this.id_tecnico = id_tecnico;
    }

    /**
     * Metodo que devuelve el id del cliente que abrio la incidencia
     * @return
     */
    public ObjectId getId_cliente() {
        return id_cliente;
    }

    /**
     * Metodo que establece el id del cliente que abrio la incidencia
     * @param id_cliente id del cliente que abrio la incidencia
     */
    public void setId_cliente(ObjectId id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * Metodo que devuelve el departamento al que pertenece la incidencia
     * @return Nombre del departamento al que pertenece la incidencia
     */
    public String getNombre_departamento_reparacion() {
        return nombre_departamento_reparacion;
    }

    /**
     * Metodo que establece el nombre del departamento al que pertenece la incidencia
     * @param nombre_departamento_reparacion Nombre del departamento
     */
    public void setNombre_departamento_reparacion(String nombre_departamento_reparacion) {
        this.nombre_departamento_reparacion = nombre_departamento_reparacion;
    }

    /**
     * Metodo que devuelve el nombre del departamento de origen de la incidencia
     * @return Nombre del departamento
     */
    public String getNombre_departamento() {
        return nombre_departamento;
    }

    /**
     * Metodo que establece el nombre del departamento de origen de la incidencia
     * @param nombre_departamento Nombre del departamamento de origen
     */
    public void setNombre_departamento(String nombre_departamento) {
        this.nombre_departamento = nombre_departamento;
    }

    /**
     * Metodo que devuelve el recambio pasado a String
     * @return String con los atributos
     */
    @Override
    public String toString() {
        return "Incidencia{" +
                "id=" + id +
                ", asunto='" + asunto + '\'' +
                ", solucion='" + solucion + '\'' +
                ", anotaciones='" + anotaciones + '\'' +
                ", estado='" + estado + '\'' +
                ", descripción='" + descripcion + '\'' +
                ", fecha_apertura=" + fecha_apertura +
                ", fecha_cierre=" + fecha_cierre +
                ", id_tecnico=" + id_tecnico +
                ", id_cliente=" + id_cliente +
                ", nombre_departamento_reparacion='" + nombre_departamento_reparacion + '\'' +
                '}';
    }
}
