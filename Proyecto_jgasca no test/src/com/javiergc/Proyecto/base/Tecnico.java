package com.javiergc.Proyecto.base;

import java.time.LocalDate;

/**
 * Clase Tecnico
 */
public class Tecnico extends Usuario {

    /**
     * Constructor de la clase Tecnico
     *
     * @param nombre Nombre del tecnico
     * @param apellidos Apellidos del tecnico
     * @param email Email del tecnico
     * @param telefono Telefono del tecnico
     * @param dni Dni del tecnico
     * @param direccion Dirección del tecnico
     * @param puesto Puesto del tecnico
     * @param contrasena Contraseña del tecnico
     * @param nombre_departamento Nombre del departamento al que pertenece el Tecnico
     * @param fechaAlta Fecha de alta del tecnico
     */
    public Tecnico(String nombre, String apellidos, String email, String telefono, String dni, String direccion, String puesto, String contrasena, String nombre_departamento, LocalDate fechaAlta) {
        super(nombre, apellidos, email, telefono, dni, direccion, puesto, contrasena, nombre_departamento, fechaAlta);
        this.setPermisos("53");
    }

    /**
     * Constructor de la clase Tecnico
     */
    public Tecnico(){}

    /**
     * Metodo que devuelve el Tecnico pasado a String
     *
     * @return String con los atributos
     */
    @Override
    public String toString() {
        return "Tecnico{" +
                "id-> " + this.getId() +
                ", nombre-> " + this.getNombre() +
                ", apellidos-> " + this.getApellidos() +
                ", email='" + this.getEmail() +
                ", telefono-> " + this.getTelefono() +
                ", dni-> " + this.getDni() +
                ", direccion-> " + this.getDireccion() +
                ", puesto-> " + this.getPuesto() +
                ", nombre_departamento-> " + this.getNombre_departamento() +
                ", fechaAlta=" + this.getFechaAlta() +
                '}';
    }
}
