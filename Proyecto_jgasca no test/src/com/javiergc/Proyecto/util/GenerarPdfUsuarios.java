package com.javiergc.Proyecto.util;

import com.javiergc.Proyecto.base.Usuario;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Clase GenerarPdfUsuarios que genera un pdf con los datos de todos los usuarios
 */
public class GenerarPdfUsuarios extends Thread {
    ArrayList<Usuario> lista = new ArrayList<>();

    /**
     * Constructor de la clase GenerarPdfUsuarios
     * @param lista lista de usuarios que apareceran en el pdf
     */
    public GenerarPdfUsuarios(ArrayList<Usuario> lista){
        this.lista = lista;
        Collections.sort(lista, new Comparator<Usuario>() {
            @Override
            public int compare(Usuario p1, Usuario p2) {
                int resultado = p1.getNombre_departamento().compareTo(p2.getNombre_departamento());
                if ( resultado != 0 ) { return resultado; }
                resultado = p1.getNombre().compareTo(p2.getNombre());
                if ( resultado != 0 ) { return resultado; }
                return resultado;
            }
        });
    }

    @Override
    public void run() {
        UsuariosDatasource datasource = new UsuariosDatasource(lista);

        JasperReport reporte = null;
        try {
            reporte = (JasperReport) JRLoader.loadObject(new File("Incidencias.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, null, datasource);
            JFileChooser selectorCarpeta = new JFileChooser();
            selectorCarpeta.setCurrentDirectory(new File("Desktop"));
            selectorCarpeta.setDialogTitle("Selecciona un carpeta en la que guardar el pdf");
            selectorCarpeta.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            selectorCarpeta.setAcceptAllFileFilterUsed(false);
            selectorCarpeta.showOpenDialog(null);

            String ruta = selectorCarpeta.getSelectedFile() + "";

            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            File file = new File(ruta + "/Usuarios.pdf");;
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE, file);

            exporter.exportReport();
            Util.showInformationAlert("Descarga realizada con exito");
        } catch (JRException e) {
            Util.showErrorAlert("Error al descargar el pdf");
            e.printStackTrace();
        }
    }
}
