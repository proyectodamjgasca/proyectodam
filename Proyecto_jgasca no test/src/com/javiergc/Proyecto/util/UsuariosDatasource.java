package com.javiergc.Proyecto.util;

import com.javiergc.Proyecto.base.Usuario;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase UsuariosDatasource
 */
public class UsuariosDatasource implements JRDataSource {
    private List<Usuario> listaUsuarios = new ArrayList<>();
    private int indiceUsuarioActual = -1;

    /**
     * Constructor de la clase UsuariosDatasource
     * @param listaUsuarios
     */
    public UsuariosDatasource(ArrayList<Usuario> listaUsuarios){
        this.listaUsuarios = listaUsuarios;
    }

    @Override
    public boolean next() throws JRException {
        return ++indiceUsuarioActual < listaUsuarios.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        Object valor = null;

        if ("nombre".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getNombre();
        }
        else if ("apellidos".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getApellidos();
        }
        else if ("email".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getEmail();
        }
        else if ("_id".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getId();
        }else if ("nombre_departamento".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getNombre_departamento();
        }else if ("dni".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getDni();
        }else if ("puesto".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getPuesto();
        }else if ("telefono".equals(jrf.getName()))
        {
            valor = listaUsuarios.get(indiceUsuarioActual).getTelefono();
        }

        return valor;
    }
}
